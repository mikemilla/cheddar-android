package app.cheddar.wallet.repository

import app.cheddar.wallet.R
import app.cheddar.wallet.model.*
import app.cheddar.wallet.utils.fetch
import com.github.kittinunf.fuel.Fuel
import app.cheddar.wallet.utils.Keys
import app.cheddar.wallet.utils.UserManager
import com.google.firebase.firestore.ListenerRegistration
import app.cheddar.wallet.utils.toModel
import com.github.kittinunf.fuel.core.extensions.jsonBody
import org.greenrobot.eventbus.EventBus
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class UsersRepository {

    fun setOnUserChangeListener(
        userId: String,
        onSuccess: (User) -> Unit,
        onFailure: (CheddarError) -> Unit
    ): ListenerRegistration? {
        return Firestore.user(userId).addSnapshotListener { snapshot, exception ->
            when {
                snapshot != null -> {
                    val user = snapshot.toModel<User>()
                    if (user != null) onSuccess(user) else {
                        val error = CheddarError(messageRes = R.string.error_content_not_found)
                        onFailure(error)
                    }
                }
                exception != null -> {
                    val error = CheddarError(exception)
                    onFailure(error)
                }
                else -> {
                    val error = CheddarError(messageRes = R.string.error_content_not_found)
                    onFailure(error)
                }
            }
        }
    }

    suspend fun getUsers(): List<User> = suspendCoroutine { cont ->
        Fuel.get("${Keys.baseUrl}/users").fetch<List<User>>(
            attachAuthToken = true,
            onSuccess = {
                val test = it
                cont.resume(it)
            },
            onFailure = { cont.resumeWithException(it) })
    }

    fun getSelf(onSuccess: (User) -> Unit, onFailure: (CheddarError) -> Unit) {
        Fuel.get("${Keys.baseUrl}/self").fetch<User>(
            attachAuthToken = true,
            onSuccess = { user ->
                UserManager.currentUser = user
                onSuccess(user)
            },
            onFailure = onFailure
        )
    }

    fun changeUsername(newUsername: String, onSuccess: (User) -> Unit, onFailure: (CheddarError) -> Unit) {
        val data = PostData(data = Username(newUsername)).toData()
        Fuel.post("${Keys.baseUrl}/change-username").jsonBody(data).fetch<User>(
            attachAuthToken = true,
            onSuccess = { user ->
                UserManager.currentUser = UserManager.currentUser?.copy(username = newUsername)
                onSuccess(user)
            },
            onFailure = onFailure
        )
    }

}

/**
 * Coroutine functions
 **/
suspend fun UsersRepository.getSelf(): User? = suspendCoroutine { cont ->
    getSelf(
        onSuccess = { cont.resume(it) },
        onFailure = { cont.resume(null) })
}