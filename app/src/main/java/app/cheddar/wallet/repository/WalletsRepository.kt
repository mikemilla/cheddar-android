package app.cheddar.wallet.repository

import app.cheddar.wallet.R
import app.cheddar.wallet.model.*
import app.cheddar.wallet.utils.fetch
import com.github.kittinunf.fuel.Fuel
import app.cheddar.wallet.utils.Keys
import com.google.firebase.firestore.ListenerRegistration
import app.cheddar.wallet.utils.toModel
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class WalletsRepository {

    fun setOnWalletChangeListener(symbol: String, onSuccess: (Wallet) -> Unit, onFailure: (CheddarError) -> Unit): ListenerRegistration? {
        var listener: ListenerRegistration? = null
        Auth.currentUser?.let { user ->
            listener = Firestore.wallet(user.uid, symbol).addSnapshotListener { snapshot, exception ->
                when {
                    snapshot != null -> {
                        val wallet = snapshot.toModel<Wallet>()
                        if (wallet != null) onSuccess(wallet) else {
                            val error = CheddarError(messageRes = R.string.error_content_not_found)
                            onFailure(error)
                        }
                    }
                    exception != null -> {
                        val error = CheddarError(exception)
                        onFailure(error)
                    }
                    else -> {
                        val error = CheddarError(messageRes = R.string.error_content_not_found)
                        onFailure(error)
                    }
                }
            }
        }
        return listener
    }

    suspend fun getTransactions(symbol: String): List<Transaction> = suspendCoroutine { cont ->
        Fuel.get("${Keys.baseUrl}/transactions?symbol=${symbol}").fetch<List<Transaction>>(
            attachAuthToken = true,
            onSuccess = { cont.resume(it) },
            onFailure = { cont.resumeWithException(it) })
    }

}