package app.cheddar.wallet.repository

import app.cheddar.wallet.App
import app.cheddar.wallet.R
import app.cheddar.wallet.model.CheddarError
import app.cheddar.wallet.model.PostData
import app.cheddar.wallet.model.Signup
import app.cheddar.wallet.model.SignupResult
import app.cheddar.wallet.utils.fetch
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.jsonBody
import app.cheddar.wallet.utils.Keys
import com.github.kittinunf.fuel.core.Request
import com.google.firebase.auth.AuthResult
import kotlinx.coroutines.*
import timber.log.Timber
import java.lang.Exception
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class AuthRepository {


}

/**
 * Coroutine functions
 **/
suspend fun AuthRepository.signup(email: String, password: String): SignupResult =
    suspendCoroutine { cont ->
        val data = PostData(data = Signup(email, password)).toData()
        Fuel.post("${Keys.baseUrl}/signup").jsonBody(data).fetch<SignupResult>(false,
            onSuccess = { cont.resume(it) },
            onFailure = { cont.resumeWithException(it) })
    }

suspend fun AuthRepository.login(email: String, password: String): AuthResult =
    suspendCoroutine { cont ->
        Auth.instance.signInWithEmailAndPassword(email, password)
            .addOnSuccessListener {
                cont.resume(it)
            }
            .addOnFailureListener { exception ->
                val error = CheddarError(exception)
                cont.resumeWithException(error)
            }
    }

suspend fun AuthRepository.login(token: String): AuthResult = suspendCoroutine { cont ->
    Auth.instance.signInWithCustomToken(token)
        .addOnSuccessListener {
            cont.resume(it)
        }
        .addOnFailureListener { exception ->
            val error = CheddarError(exception)
            cont.resumeWithException(error)
        }
}