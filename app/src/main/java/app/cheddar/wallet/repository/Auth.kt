package app.cheddar.wallet.repository

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

object Auth {

    val isSignedIn: Boolean get() = currentUser != null
    val currentUser: FirebaseUser? get() = instance.currentUser
    val instance get() = FirebaseAuth.getInstance()

    fun signOut() = GlobalScope.launch {
        instance.signOut()
    }
}