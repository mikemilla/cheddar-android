package app.cheddar.wallet.repository

import app.cheddar.wallet.R
import app.cheddar.wallet.model.*
import app.cheddar.wallet.utils.UserManager
import app.cheddar.wallet.utils.getSnapshot
import com.google.firebase.firestore.ListenerRegistration
import app.cheddar.wallet.utils.toModel
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class ChainsRepository {

    fun setOnChainChangeListener(symbol: String, onSuccess: (Chain) -> Unit, onFailure: (CheddarError) -> Unit): ListenerRegistration? {
        return Firestore.chain(symbol).addSnapshotListener { snapshot, exception ->
            when {
                snapshot != null -> {
                    val chain = snapshot.toModel<Chain>()
                    if (chain != null) onSuccess(chain) else {
                        val error = CheddarError(messageRes = R.string.error_content_not_found)
                        onFailure(error)
                    }
                }
                exception != null -> {
                    val error = CheddarError(exception)
                    onFailure(error)
                }
                else -> {
                    val error = CheddarError(messageRes = R.string.error_content_not_found)
                    onFailure(error)
                }
            }
        }
    }

    fun getChain(symbol: String, onSuccess: (Chain?) -> Unit, onFailure: (CheddarError) -> Unit) {
        Firestore.chain(symbol).getSnapshot<Chain>()
            .addOnSuccessListener { chain ->
                UserManager.recentBitcoinPrice = chain?.price // TODO
                onSuccess(chain)
            }
            .addOnFailureListener { exception ->
                val error = CheddarError(exception, R.string.error_get_chain)
                onFailure(error)
            }
    }

}

suspend fun ChainsRepository.getChain(symbol: String): Chain? = suspendCoroutine { cont ->
    getChain(symbol,
        onSuccess = { cont.resume(it) },
        onFailure = { cont.resume(null) })
}