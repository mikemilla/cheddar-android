package app.cheddar.wallet.repository

import app.cheddar.wallet.utils.Keys
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings

object Firestore {

    private val firestore get() = FirebaseFirestore.getInstance()

    fun build() {
        val settings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .build()
        firestore.firestoreSettings = settings
    }

    fun user(userId: String) = firestore.collection("users").document(userId)
    fun wallet(userId: String, symbol: String) = firestore.collection("users/${userId}/wallets").document(symbol)
    fun chain(symbol: String) = firestore.collection("chains").document(symbol)
}