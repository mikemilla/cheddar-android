package app.cheddar.wallet.utils

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import app.cheddar.wallet.R

// This file contains all the resources we reference using ContextCompat
// If you find yourself getting / setting an asset programmatically, think
// of getting it from here. If it doesn't exist, please add it here and use
// this to reference it. Thanks! 👨‍🎨

/** Extensions **/
fun Context.findDrawable(resId: Int) = ContextCompat.getDrawable(this, resId)!!
fun Context.findColor(resId: Int) = ContextCompat.getColor(this, resId)
fun Context.findFont(resId: Int) = ResourcesCompat.getFont(this, resId)

/** Colors **/
val Context.colorPrimary get() = findColor(R.color.colorPrimary)
val Context.colorPrimaryDark get() = findColor(R.color.colorPrimaryDark)
val Context.colorDarkGray get() = findColor(R.color.colorDarkGray)
val Context.colorBackgroundInverse get() = findColor(R.color.colorBackgroundInverse)
val Context.colorShadow get() = findColor(R.color.colorShadow)

/** Drawables **/
//val Context.drawableAlarmIcon get() = findDrawable(R.drawable.ic_alarm)
//val Context.drawableTimerIcon get() = findDrawable(R.drawable.ic_timer)

/** Icons & Images **/
val Context.iconBack get() = findDrawable(R.drawable.ic_back)
//val Context.iconHeartEmpty get() = findDrawable(R.drawable.ic_heart_empty)
//val Context.iconHeartFull get() = findDrawable(R.drawable.ic_heart_full)

/** Dimens **/
val Context.dimen2 get() = this.resources.getDimension(R.dimen.dimen2)
val Context.dimen4 get() = this.resources.getDimension(R.dimen.dimen4)
val Context.dimen8 get() = this.resources.getDimension(R.dimen.dimen8)
val Context.dimen16 get() = this.resources.getDimension(R.dimen.dimen16)
val Context.dimen56 get() = this.resources.getDimension(R.dimen.dimen56)
val Context.dimen88 get() = this.resources.getDimension(R.dimen.dimen88)
val Context.dimenText20 get() = this.resources.getDimension(R.dimen.text20)

/** Fonts **/
val Context.fontRegular get() = findFont(R.font.sofia_pro_regular)
val Context.fontMedium get() = findFont(R.font.sofia_pro_medium)
val Context.fontBold get() = findFont(R.font.sofia_pro_bold)