package app.cheddar.wallet.utils

import app.cheddar.wallet.R
import app.cheddar.wallet.model.CheddarError
import app.cheddar.wallet.repository.Auth
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.*
import timber.log.Timber
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


inline fun <reified T : Any> Request.fetch(
    attachAuthToken: Boolean = true,
    crossinline onSuccess: (T) -> Unit,
    crossinline onFailure: (CheddarError) -> Unit
) = GlobalScope.launch {

    // Fetch the last auth token if needed
    if (attachAuthToken) {
        try {
            val token = getAuthToken()
            this@fetch.header("authorization", "Bearer $token")
        } catch (e: CheddarError) {
            onFailure(e)
            return@launch
        }
    }

    responseObject(Deserializer()) { _, _, result ->
        val content = result.component1()
        val error = result.component2()
        when {
            error != null -> {
                val exception = Exception(error.message)
                val cheddarError = CheddarError(exception)
                onFailure(cheddarError)
            }
            content == null -> {
                val cheddarError = CheddarError(messageRes = R.string.error_content_not_found)
                onFailure(cheddarError)
            }
            else -> {
                val value = Gson().fromJson<T>(content)
                onSuccess(value)
            }
        }
    }
}

// Fetches the latest auth token
suspend fun getAuthToken(): String? = suspendCoroutine { cont ->
    val tokenFetch = Auth.currentUser?.getIdToken(true)
    if (tokenFetch == null) {
        val error = CheddarError(messageRes = R.string.error_invalid_auth_token)
        cont.resumeWithException(error)
    } else {
        tokenFetch
            .addOnSuccessListener {
                cont.resume(it.token)
            }
            .addOnFailureListener { err ->
                val exception = Exception(err.message)
                val error = CheddarError(exception)
                cont.resumeWithException(error)
            }
    }
}

inline fun <reified T> typeToken() = object : TypeToken<T>() {}.type
inline fun <reified T> Gson.fromJson(json: String): T = fromJson(json, typeToken<T>())
class Deserializer : ResponseDeserializable<String> {
    override fun deserialize(content: String): String = content
}

enum class Source { CACHE, SERVER }