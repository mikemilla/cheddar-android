package app.cheddar.wallet.utils

import android.animation.Animator
import android.view.View

object AnimUtils {

    const val DEFAULT_DURATION: Long = 75

    // Scales view down with an animation
    fun scaleViewDown(view: View, scale: Float = 0.9f, alpha: Float = 0.6f) {
        view.animate()
            .scaleX(scale)
            .scaleY(scale)
            .alpha(alpha)
            .setDuration(DEFAULT_DURATION)
            .start()
    }

    // Scales view down with an animation
    fun scaleViewUp(view: View, onAnimationEnd: (() -> Unit)? = null) {
        view.animate()
            .scaleX(1f)
            .scaleY(1f)
            .alpha(1f)
            .setDuration(DEFAULT_DURATION)
            .setListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animator: Animator?) {}
                override fun onAnimationCancel(animator: Animator?) {}
                override fun onAnimationStart(animator: Animator?) {}
                override fun onAnimationEnd(animator: Animator?) {
                    onAnimationEnd?.invoke()
                }
            })
            .start()
    }

    // Slides towards
    fun slideIn(view: View, startY: Float = -view.context.dimen16, endAlpha: Float = 1f, onAnimationEnd: (() -> Unit)? = null) {
        view.translationY = startY
        view.alpha = 0f
        view.animate()
            .translationY(0f)
            .alpha(endAlpha)
            .setDuration(DEFAULT_DURATION)
            .setListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animator: Animator?) {}
                override fun onAnimationCancel(animator: Animator?) {}
                override fun onAnimationStart(animator: Animator?) {}
                override fun onAnimationEnd(animator: Animator?) {
                    onAnimationEnd?.invoke()
                }
            })
            .start()
    }

    // Slides away
    fun slideOut(view: View, endY: Float = -view.context.dimen16, startAlpha: Float = 1f, onAnimationEnd: (() -> Unit)? = null) {
        view.translationY = 0f
        view.alpha = startAlpha
        view.animate()
            .translationY(endY)
            .alpha(0f)
            .setDuration(DEFAULT_DURATION)
            .setListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animator: Animator?) {}
                override fun onAnimationCancel(animator: Animator?) {}
                override fun onAnimationStart(animator: Animator?) {}
                override fun onAnimationEnd(animator: Animator?) {
                    onAnimationEnd?.invoke()
                }
            })
            .start()
    }

}