package app.cheddar.wallet.utils

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.app.Activity
import android.app.PendingIntent.getActivity
import android.content.Context
import android.content.res.Resources
import android.os.Handler
import android.view.*
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.cheddar.wallet.App
import app.cheddar.wallet.R
import app.cheddar.wallet.model.CheddarError
import app.cheddar.wallet.model.CheddarModel
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.DocumentReference
import org.greenrobot.eventbus.EventBus
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Source
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import timber.log.Timber
import java.text.NumberFormat
import java.util.*

const val DEFAULT_VIEW_ANIMATION_DURATION = 300L
const val DEFAULT_IMAGE_ANIMATION_DURATION = 100
const val MAX_SCROLL_VIEW_ELEVATION_IN_DP = 4f
const val MAX_SCROLL_ELEVATION_DISTANCE = 16f

fun Float.toScaledPX(ctx: Context): Float = this * ctx.resources.displayMetrics.scaledDensity
fun Float.toDp(context: Context): Float = this / context.resources.displayMetrics.density
fun Float.toPx(context: Context): Float = this * context.resources.displayMetrics.density
fun Int.toScaledPX(ctx: Context): Float = this * ctx.resources.displayMetrics.scaledDensity
fun Int.toDp(context: Context): Float = this / context.resources.displayMetrics.density
fun Int.toPx(context: Context): Float = this * context.resources.displayMetrics.density

fun Double.toDeviceCurrency(showFractionsOfPenny: Boolean = true): String {
    return NumberFormat.getCurrencyInstance(Locale.getDefault()).apply {
        val symbol = UserManager.defaultFiatCurrency.toUpperCase()
        currency = Currency.getInstance(symbol)
        if (showFractionsOfPenny) maximumFractionDigits = App.MAX_CURRENCY_DECIMAL_PLACES
    }.format(this)
}

fun Context.toastTodo() = Toast.makeText(
    this,
    "Coming Soon! \uD83D\uDE18\uD83D\uDC49\uD83D\uDCF1\uD83D\uDC49\uD83D\uDCAA",
    Toast.LENGTH_SHORT
).show()

fun delay(delay: Long, function: () -> Unit) = Handler().postDelayed(function, delay)

fun ViewGroup.inflate(layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}

fun ViewGroup.inflateAndAttach(layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, true)
}

fun View.show(animated: Boolean = false, toAlpha: Float = 1f, onComplete: (() -> Unit)? = null) {
    if (animated) {
        if (visibility != View.VISIBLE) {
            alpha = 0f
            visibility = View.VISIBLE
            isEnabled = true
        }
        animate()
            .setDuration(DEFAULT_VIEW_ANIMATION_DURATION)
            .alpha(toAlpha)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    onComplete?.invoke()
                }
            })
    } else {
        alpha = toAlpha
        isEnabled = true
        visibility = View.VISIBLE
        onComplete?.invoke()
    }
}

fun View.hide(animated: Boolean = false, onComplete: (() -> Unit)? = null) {
    if (animated) {
        animate()
            .setDuration(DEFAULT_VIEW_ANIMATION_DURATION)
            .alpha(0f)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    visibility = View.GONE
                    onComplete?.invoke()
                }
            })
    } else {
        visibility = View.GONE
        onComplete?.invoke()
    }
}

fun View.makeInvisible(animated: Boolean = false, onComplete: (() -> Unit)? = null) {
    if (animated) {
        animate()
            .setDuration(DEFAULT_VIEW_ANIMATION_DURATION)
            .alpha(0f)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    visibility = View.INVISIBLE
                    onComplete?.invoke()
                }
            })
    } else {
        visibility = View.INVISIBLE
        onComplete?.invoke()
    }
}

fun View.shake() {
    val anim = TranslateAnimation(-context.dimen2, context.dimen2, 0f, 0f).apply {
        duration = 20L
        repeatMode = Animation.REVERSE
        repeatCount = 3
    }
    startAnimation(anim)
}

fun View.press(scale: Float = 0.9f) {
    setOnTouchListener { view, event ->
        when (event?.action) {
            MotionEvent.ACTION_DOWN,
            MotionEvent.ACTION_BUTTON_PRESS -> {
                AnimUtils.scaleViewDown(this, scale)
            }
            MotionEvent.ACTION_UP,
            MotionEvent.ACTION_CANCEL,
            MotionEvent.ACTION_BUTTON_RELEASE -> {
                AnimUtils.scaleViewUp(this)
            }
        }
        return@setOnTouchListener onTouchEvent(event)
    }
}

fun View.fillEdgeToEdge(onFilled: (top: Int, bottom: Int) -> Unit) {
    systemUiVisibility =
        View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
    setOnApplyWindowInsetsListener { view, insets ->
        val top = insets.systemWindowInsetTop
        val bottom = insets.systemWindowInsetBottom
        onFilled(top, bottom)
        return@setOnApplyWindowInsetsListener insets.consumeSystemWindowInsets()
    }
}

fun View.animateHeightChange(newHeight: Float, duration: Long = DEFAULT_VIEW_ANIMATION_DURATION) {
    ValueAnimator.ofInt(height, newHeight.toInt()).apply {
        this.duration = duration
        this.addUpdateListener {
            val animatedValue = animatedValue as Int
            val params = layoutParams.apply {
                height = animatedValue
            }
            layoutParams = params
        }
        start()
    }
}

fun View.hideKeyboard(): Boolean {
    clearFocus()
    val inputMethodManager =
        context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    return inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
}

fun EditText.showKeyboard() {
    if (requestFocus()) {
        (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
        setSelection(text.length)
    }
}

fun EditText.moveCursorToEnd() {
    setSelection(text.length)
}

fun EventBus.registerIfNeeded(subscriber: Any) {
    try {
        if (!isRegistered(subscriber)) register(subscriber)
    } catch (e: Exception) {
    }
}

fun EventBus.unregisterIfNeeded(subscriber: Any) {
    try {
        if (isRegistered(subscriber)) unregister(subscriber)
    } catch (e: Exception) {
    }
}

fun <T> MutableLiveData<T>.notifyObservers() = postValue(value)

fun <T> MutableLiveData<T>.postIfNeeded(newValue: T?) {
    if (value != newValue) postValue(newValue)
}

fun CheddarError.toMessage(): String {
    return exception.message.toString()
}

fun Double?.toCleanNumber(): String {
    return (this ?: 0.0).toBigDecimal().toPlainString()
}

fun getFiatForAmount(amount: Double): String {
    val format = NumberFormat.getCurrencyInstance().apply {
        maximumFractionDigits = 4
        minimumFractionDigits = 0
        val symbol = UserManager.defaultFiatCurrency
        currency = Currency.getInstance(symbol)
    }
    return format.format(amount)
}

fun Resources.getPluralForToken(tokenName: String?, amount: Double): String {
    val count = if (amount != 1.0) 0 else 1
    return getQuantityString(R.plurals.plural_tokens, count, amount.toCleanNumber(), tokenName)
}

fun Resources.getPluralForTokenNoCount(tokenName: String?, amount: Double): String {
    val count = if (amount != 1.0) 0 else 1
    return getQuantityString(
        R.plurals.plural_tokens_no_count,
        count,
        amount.toCleanNumber(),
        tokenName
    )
}

// Gets a viewHolder at an index based on the recyclerView reference
// Will return null of the viewHolder is not in view or doesn't exist
@Suppress("UNCHECKED_CAST")
fun <T> RecyclerView.getViewHolderAtIndex(index: Int): T? {
    return try {
        findViewHolderForAdapterPosition(index) as? T
    } catch (e: Exception) {
        Timber.e(e)
        null
    }
}

fun RecyclerView.getAllVisibleIndexes(): List<Int> {
    return try {
        when (layoutManager) {
            is LinearLayoutManager -> {
                val manager = (layoutManager as LinearLayoutManager)
                val firstIndex = manager.findFirstVisibleItemPosition()
                val lastIndex = manager.findLastVisibleItemPosition()
                val indexes = mutableListOf<Int>()
                for (i in firstIndex until lastIndex + 1) indexes.add(i)
                return indexes
            }
            else -> emptyList()
        }
    } catch (e: Exception) {
        Timber.e(e)
        emptyList()
    }
}

// This will prevent invalid types of values from crashing the app
inline fun <reified T> DocumentSnapshot.toModel(): T? {

    // Parse
    val obj = try {
        toObject(T::class.java)
    } catch (e: Exception) {
        Timber.e(e)
        null
    }

    // Add id if needed
    if (obj is CheddarModel) {
        obj.id = this.id
    }

    return obj
}

inline fun <reified T : CheddarModel> DocumentReference.getSnapshot(source: Source = Source.DEFAULT): Task<T?> {
    return get(source).onSuccessTask { snapshot ->
        if (snapshot != null) Tasks.call {
            return@call snapshot.toModel<T>()
        }
        else Tasks.forException(Exception("Document not found"))
    }
}

// Converts models into string
inline fun <reified T> T?.toJson(): String? {
    try {
        return Gson().toJson(this)
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return null
}

// Converts strings into models
inline fun <reified T> String?.parse(): T? {
    try {
        val listType = object : TypeToken<T>() {}.type
        return Gson().fromJson<T>(this, listType)
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return null
}
