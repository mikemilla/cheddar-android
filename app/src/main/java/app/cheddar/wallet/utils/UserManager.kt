package app.cheddar.wallet.utils

import android.content.Context
import app.cheddar.wallet.App
import app.cheddar.wallet.events.CurrencyChangeEvent
import app.cheddar.wallet.events.UserChangeEvent
import app.cheddar.wallet.model.Price
import app.cheddar.wallet.model.User
import com.google.gson.Gson
import org.greenrobot.eventbus.EventBus

object UserManager {

    private val preferences
        get() = App.instance.getSharedPreferences(
            "preferences",
            Context.MODE_PRIVATE)

    var currentUser: User?
        get() = preferences.getString(Keys.currentUser, null).parse<User>()
        set(value) {

            val json = Gson().toJson(value)
            preferences
                .edit()
                .putString(Keys.currentUser, json)
                .apply()

            // Post the username change event
            value?.let { user ->
                val event = UserChangeEvent(user)
                EventBus.getDefault().post(event)
            }
        }

    var defaultFiatCurrency: String
        get() = preferences.getString(Keys.defaultCurrency, Keys.usd)!!
        set(value) {

            preferences
                .edit()
                .putString(Keys.defaultCurrency, value)
                .apply()

            // Post event
            val event = CurrencyChangeEvent(value)
            EventBus.getDefault().post(event)
        }

    var recentBitcoinPrice: Price?
        get() = preferences.getString(Keys.bitcoinPrice, null).parse<Price>()
        set(value) {
            val json = Gson().toJson(value)
            preferences
                .edit()
                .putString(Keys.bitcoinPrice, json)
                .apply()
        }

    fun reset() {
        defaultFiatCurrency = Keys.usd
        recentBitcoinPrice = null
        currentUser = null
    }

}