package app.cheddar.wallet.utils

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.snackbar.Snackbar
import org.jetbrains.anko.dimen


object SnackBarUtils {

    data class Action(val titleRes: Int, val onClick: () -> Unit)

    fun present(context: Context?, containerLayout: View, messageRes: Int, bottomMargin: Int = 0, action: Action? = null) {
        context?.let {
            Snackbar.make(containerLayout, messageRes, Snackbar.LENGTH_LONG).apply {
                build(context, this, bottomMargin, action)
                show()
            }
        }
    }

    fun present(context: Context?, containerLayout: View, message: String, bottomMargin: Int = 0, action: Action? = null) {
        context?.let {
            Snackbar.make(containerLayout, message, Snackbar.LENGTH_LONG).apply {
                build(context, this, bottomMargin, action)
                show()
            }
        }
    }

    private fun build(context: Context, snackBar: Snackbar, bottomMargin: Int = 0, action: Action? = null) {
        snackBar.apply {
            view.setPadding(0 , 0, 0, bottomMargin)
            view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text).apply {
                this.typeface = context.fontMedium
                this.textSize = 16f
                this.setLineSpacing(1f, 1.2f)
            }
            action?.let {
                setAction(action.titleRes) { action.onClick.invoke() }
                view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text).apply {
                    this.typeface = context.fontMedium
                    this.textSize = 16f
                    this.setLineSpacing(1f, 1.2f)
                }
            }
        }
    }

}