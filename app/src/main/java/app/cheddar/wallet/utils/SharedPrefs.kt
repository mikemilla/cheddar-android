package app.cheddar.wallet.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.content.SharedPreferences
import app.cheddar.wallet.App

object SharedPrefs {

    private const val TIMER_SECONDS = "timer_seconds"
    private const val IS_SUBSCRIBER = "is_subscriber"
    private const val CURRENT_COLOR = "current_color"

    private val prefs: SharedPreferences
        get() {
            App.instance.apply {
                return getSharedPreferences(packageName, Activity.MODE_PRIVATE)
            }
        }

    var timerSecondsDuration: Long
        get() = prefs.getLong(TIMER_SECONDS, 21600) // 6 Hours
        set(value) = prefs.edit().putLong(TIMER_SECONDS, value).apply()

    var isSubscriber: Boolean
        get() = prefs.getBoolean(IS_SUBSCRIBER, false)
        @SuppressLint("ApplySharedPref")
        set(value) {
            prefs.edit().putBoolean(IS_SUBSCRIBER, value).commit()
        }

}