package app.cheddar.wallet.utils

object Keys {

    val baseUrl = "http://35.224.57.157"
    val users = "users"
    val wallets = "wallets"
    val btcReg = "btc_reg"
    val bitcoinPrice = "bitcoin_price"
    val defaultCurrency = "default_currency"
    val currentUser = "current_user"
    val usd = "usd"

}