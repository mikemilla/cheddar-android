package app.cheddar.wallet.view

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.LinearLayout
import android.graphics.*
import android.view.Gravity
import androidx.core.view.ViewCompat
import app.cheddar.wallet.R
import app.cheddar.wallet.utils.*
import app.cheddar.wallet.utils.inflateAndAttach
import kotlinx.android.synthetic.main.view_cheddar_button.view.*

open class CheddarButton : LinearLayout {

    companion object {
        private const val ANIM_DURATION = 100L
    }

    var isDisabled: Boolean = false
        set(value) {
            field = value
            isEnabled = !isDisabled
            alpha = if (isEnabled) 1f else 0.4f
        }

    var title: String? = null
        set(value) {
            field = value
            titleTextView.text = title
        }

    private val chunkSize = DimenUtils.dpToPx(6f, context)
    private var canvas: Canvas? = null

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {

        inflateAndAttach(R.layout.view_cheddar_button)
        ViewCompat.generateViewId()
        setBackgroundColor(context.colorPrimary)
        orientation = HORIZONTAL
        gravity = Gravity.CENTER

        context.theme.obtainStyledAttributes(attrs, R.styleable.CheddarButton, defStyle, 0).apply {
            try {

                title = getString(R.styleable.CheddarButton_title)
                setBackgroundColor(
                    getColor(
                        R.styleable.CheddarButton_backgroundColor,
                        context.colorPrimary
                    )
                )
                val tintColor = getColor(R.styleable.CheddarButton_tintColor, context.colorDarkGray)
                titleTextView.setTextColor(tintColor)
                // TODO: TINT IMAGE

                isDisabled = getBoolean(R.styleable.CheddarButton_isDisabled, false)

            } finally {
                recycle()
            }
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {

        // Touch location
        val x = event?.x
        val y = event?.y

        // Event
        when (event?.action) {
            MotionEvent.ACTION_DOWN,
            MotionEvent.ACTION_BUTTON_PRESS -> {
                AnimUtils.scaleViewDown(this)
            }
            MotionEvent.ACTION_CANCEL -> {
                AnimUtils.scaleViewUp(this)
            }
            MotionEvent.ACTION_UP,
            MotionEvent.ACTION_BUTTON_RELEASE -> {
                AnimUtils.scaleViewUp(this)
                if (x != null && y != null) {
                    val containerRect = Rect(0, 0, width, height)
                    if (containerRect.contains(x.toInt(), y.toInt())) {
                        performClick()
                        return true
                    }
                }
            }
        }

        return super.onTouchEvent(event)
    }

    override fun performClick(): Boolean {
        return super.performClick()
    }

//    override fun onDraw(canvas: Canvas?) {
//        super.onDraw(canvas)
//        this.canvas = canvas
//        this.canvas?.drawCheese()
//    }
//
//    private fun Canvas.drawCheese(isSelected: Boolean = false) {
//
//        drawColor(Color.TRANSPARENT)
//
//        val paint = Paint().apply {
//            style = Paint.Style.FILL
//            color = if (isSelected) context.colorPrimaryDark else context.colorPrimary
//        }
//
//        // Dimens
//        val w = this.width.toFloat()
//        val h = this.height.toFloat()
//        val x1 = MetricsUtil.convertDpToPixel(28f, context)
//        val x2 = x1 + chunkSize
//        val x3 = x2 + chunkSize
//
//        // Path
//        val chunkPath = Path().apply {
//            moveTo(0f, 0f)
//            lineTo(x1, 0f)
//            lineTo(x2, chunkSize)
//            lineTo(x3, 0f)
//            lineTo(w, 0f)
//            lineTo(w, h)
//            lineTo(0f, h)
//            close()
//        }
//
//        drawPath(chunkPath, paint)
//    }
//
//    override fun performClick(): Boolean {
//        super.performClick()
//        return true
//    }
//
//    override fun onTouchEvent(event: MotionEvent?): Boolean {
//        return when (event?.action) {
//            MotionEvent.ACTION_DOWN -> {
//                this.canvas?.drawCheese(true)
//                animate().cancel()
//                animate()
//                    .scaleXBy(-0.05f)
//                    .scaleYBy(-0.05f)
//                    .setDuration(ANIM_DURATION)
//                    .start()
//                true
//            }
//            MotionEvent.ACTION_CANCEL -> {
//                this.canvas?.drawCheese(false)
//                animate().cancel()
//                animate()
//                    .scaleX(1f)
//                    .scaleY(1f)
//                    .setDuration(ANIM_DURATION)
//                    .start()
//                true
//            }
//            MotionEvent.ACTION_UP -> {
//                this.canvas?.drawCheese(false)
//                animate().cancel()
//                animate()
//                    .scaleX(1f)
//                    .scaleY(1f)
//                    .setDuration(ANIM_DURATION)
//                    .start()
//                performClick()
//                true
//            }
//            else -> false
//        }
//    }
}