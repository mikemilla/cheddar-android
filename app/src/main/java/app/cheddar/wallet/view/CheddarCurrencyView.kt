package app.cheddar.wallet.view

import android.animation.ValueAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.FrameLayout
import android.widget.NumberPicker
import android.widget.TextView
import androidx.transition.AutoTransition
import androidx.transition.Transition
import androidx.transition.TransitionManager
import app.cheddar.wallet.R
import app.cheddar.wallet.model.Balance
import app.cheddar.wallet.model.Chain
import app.cheddar.wallet.model.Price
import app.cheddar.wallet.utils.*
import kotlinx.android.synthetic.main.fragment_send_amount.view.*
import kotlinx.android.synthetic.main.view_cheddar_currency.view.*
import kotlinx.android.synthetic.main.view_cheddar_currency.view.amountContainer
import timber.log.Timber
import java.util.*

open class CheddarCurrencyView : FrameLayout {

    companion object {
        const val ANIM_DURATION = 100L
        private const val LARGE_FONT_SIZE = 54f
        private const val SMALL_FONT_SIZE = 24f
        private const val NORMAL_CHARS = "1234567890,."
        private const val MAX_INTEGER_PLACES = 6
        private const val MAX_CURRENCY_PLACES = 4
        private const val MAX_CRYPTO_PLACES = 8
    }

    private val ANIM_TRANS_Y = context.dimen8

    private var textViews = mutableListOf<TextView>()
    private var defaultTextView: TextView? = null
    private var didStart = false

    private var onValueChangeListener: ((Double) -> Unit)? = null

    private var currentValue: String = ""
        set(value) {
            field = value
            onValueChangeListener?.invoke(this.value)
        }

    val value: Double
        get() {
            return if (currentValue.isEmpty() || currentValue == ".") 0.0
            else currentValue.filter { NORMAL_CHARS.contains(it) }.toDouble()
        }

    enum class Size { LARGE, SMALL }
    var size = Size.LARGE
        set(value) {
            if (field != value) {
                field = value
                resize()
            }
        }

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        inflateAndAttach(R.layout.view_cheddar_currency)

        context.theme.obtainStyledAttributes(attrs, R.styleable.CheddarCurrencyView, defStyle, 0)
            .apply {
                try {
                    val sizeIndex = getInteger(R.styleable.CheddarCurrencyView_size, 0)
                    size = Size.values()[sizeIndex]
                } finally {
                    recycle()
                }
            }

        // TODO: HANDLE ANIMATE ON LAUNCH

//        if (isInEditMode) {
//            val price = Price(usd = FiatCurrency(amount = 20000.00))
//            val balance = Balance(symbol = "BTC", totalBalance = 0.123, availableBalance = 0.123)
//            update(price, balance, false)
//        }

        addDefaultTextView()
        didStart = true
        resize()
    }

    fun setOnValueChangeListener(onValueChangeListener: (Double) -> Unit) {
        this.onValueChangeListener = onValueChangeListener
    }

    fun setIcon(iconUrl: String) {
        // TODO
    }

    // Sets the view to the price and balance provided
    // Ensure the user doesn't see an inaccurate balance
    fun update(price: Price?, balance: Balance?, animated: Boolean) {

        // Get the new value
        val newValue =
            if (price == null || balance == null) context.getString(R.string.copy_error) else {
                price.currentFiatCurrency?.toValue(balance.totalBalance).toString().filter {
                    NORMAL_CHARS.contains(it)
                }
            }

        // Handle special characters

        // Only update if we need to
        if (newValue != currentValue) {
            currentValue = newValue
            hide(animated) {
                defaultTextView?.setTextColor(context.colorBackgroundInverse)
                defaultTextView?.text = currentValue
                show(animated)
            }
        }
    }

    private fun makeTextView(value: String = ""): TextView {
        val size = if (size == Size.LARGE) LARGE_FONT_SIZE else SMALL_FONT_SIZE
        val viewStyle = if (this.size == Size.LARGE) R.style.Text_Bold else R.style.Text_Regular
        return TextView(context, null, 0, viewStyle).apply {
            id = View.generateViewId()
            textSize = size
            setTextColor(context.colorBackgroundInverse)
            layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
            text = value
            isAllCaps = false
        }
    }

    fun setValue(value: String) {

        // New value matches previous
        if (currentValue == value) {
            containerLayout.shake()
            return
        }

        // Set new value
        currentValue = ""
        textViews.forEach {
            amountContainer.removeView(it)
        }
        textViews.clear()
        value.forEach {
            defaultTextView?.setTextColor(context.colorBackgroundInverse)
            attachCharacter(it.toString(), false)
        }
    }

    fun addCharacter(character: String) {

        // Get places from decimal
        val indexOfDec = currentValue.indexOf(".")
        val placesBeforeDecimal =
            if (indexOfDec == -1) currentValue.length
            else currentValue.substring(0, indexOfDec).length
        val placesAfterDecimal =
            if (indexOfDec == -1) 0
            else currentValue.substring(indexOfDec).length

        // Handle special cases
        when {
            character == "." && currentValue.contains(".") -> {
                containerLayout.shake()
            }
            character == "0" && currentValue.isEmpty() -> {
                containerLayout.shake()
            }
            placesBeforeDecimal >= MAX_INTEGER_PLACES && character != "." && !currentValue.contains(
                "."
            ) -> {
                containerLayout.shake()
            }
            placesAfterDecimal > MAX_CURRENCY_PLACES -> {
                containerLayout.shake()
            }
            character == "." && currentValue.isEmpty() -> {
                defaultTextView?.setTextColor(context.colorBackgroundInverse)
                attachCharacter(character, true)
            }
            else -> {
                removeDefaultTextView()
                attachCharacter(character, true)
            }
        }

    }

    fun removeCharacter() {

        if (currentValue.isEmpty()) {
            containerLayout.shake()
            return
        }

        detachCharacter()
    }

    private fun attachCharacter(character: String, isAnimated: Boolean = true) {

        // Build a textView
        val textView = makeTextView(character).apply {
            alpha = if (isAnimated) 0f else 1f
        }

        // Update globals
        textViews.add(textView)
        val indexToAddAt = amountContainer.indexOfChild(chainTitleTextView)
        amountContainer.addView(textView, indexToAddAt)
        currentValue += character

        // Animate in
        if (isAnimated) {
            animateWidthChange {
                AnimUtils.slideIn(
                    view = textView,
                    startY = ANIM_TRANS_Y,
                    endAlpha = 1f
                )
            }
        }
    }

    private fun detachCharacter(isAnimated: Boolean = true) {
        currentValue = currentValue.dropLast(1)
        textViews.lastOrNull()?.let { textView ->
            if (isAnimated) {
                AnimUtils.slideOut(
                    view = textView,
                    endY = ANIM_TRANS_Y,
                    onAnimationEnd = {
                        amountContainer.removeView(textView)
                        textViews.remove(textView)
                        addDefaultTextView()
                        animateWidthChange()
                    })
            } else {
                amountContainer.removeView(textView)
                textViews.remove(textView)
                addDefaultTextView()
                animateWidthChange()
            }
        }
    }

    private fun addDefaultTextView() {
        if (currentValue.isEmpty()) {

            // Add if needed
            if (defaultTextView == null) {
                defaultTextView = makeTextView("0")
                textViews.add(defaultTextView!!)
                val indexToAddAt = amountContainer.indexOfChild(chainTitleTextView)
                amountContainer.addView(defaultTextView, indexToAddAt)
            }

            // Ensure proper alpha
            defaultTextView?.setTextColor(context.colorShadow)
        }
    }

    private fun removeDefaultTextView() {
        if (currentValue.isEmpty() && defaultTextView != null) {
            textViews.remove(defaultTextView!!)
            amountContainer.removeView(defaultTextView)
            defaultTextView = null
        }
    }

    private fun animateWidthChange(onTransitionComplete: (() -> Unit)? = null) {
        val transition = AutoTransition().apply { duration = ANIM_DURATION }
            .addListener(object : Transition.TransitionListener {
                override fun onTransitionEnd(transition: Transition) {
                    onTransitionComplete?.invoke()
                }

                override fun onTransitionResume(transition: Transition) {}
                override fun onTransitionPause(transition: Transition) {}
                override fun onTransitionCancel(transition: Transition) {}
                override fun onTransitionStart(transition: Transition) {}
            })
        TransitionManager.beginDelayedTransition(containerLayout, transition)
    }

    private fun resize() {
        with(amountContainer) {
            for (i in 0 until childCount) {
                val viewAtIndex = getChildAt(i)
                if (viewAtIndex is TextView) {
                    viewAtIndex.updateSize()
                }
            }
        }
    }

    private fun TextView.updateSize() {

        // Get styles
        val newStyleIsLarge = size == Size.LARGE

        // Set text
        typeface = if (newStyleIsLarge) context.fontBold else context.fontRegular

        // Get new sizes
        val startSize = if (newStyleIsLarge) SMALL_FONT_SIZE else LARGE_FONT_SIZE
        val endSize = if (newStyleIsLarge) LARGE_FONT_SIZE else SMALL_FONT_SIZE

        // Perform animation
        ValueAnimator.ofFloat(startSize, endSize).apply {
            duration = if (didStart) ANIM_DURATION else 0L
            addUpdateListener { valueAnimator ->
                val animatedValue = valueAnimator.animatedValue as Float
                this@updateSize.textSize = animatedValue
            }
            start()
        }
    }

}