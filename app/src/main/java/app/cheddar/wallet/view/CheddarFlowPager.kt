package app.cheddar.wallet.view

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.viewpager.widget.ViewPager
import kotlin.math.abs

class CheddarFlowPager @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : ViewPager(context, attrs) {

    var isSwipable = false

    init {
        setPageTransformer(false, CheddarFlowPageTransformer())
    }

    inner class CheddarFlowPageTransformer : PageTransformer {
        override fun transformPage(view: View, position: Float) {

            // Fades at half and fades in
            view.alpha = 1 - (abs(position) * 2)

            // Handle translation
            val parallaxPosition = (position * 0.5f)
            view.translationX = when {
                position <= -1f || position >= 1f -> view.width.toFloat() * parallaxPosition
                position == 0f -> view.width.toFloat() * parallaxPosition
                else -> view.width.toFloat() * -parallaxPosition
            }

        }
    }

    override fun onTouchEvent(ev: MotionEvent?): Boolean {
        return isSwipable && super.onTouchEvent(ev)
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        return isSwipable && super.onInterceptTouchEvent(ev)
    }

    override fun canScrollHorizontally(direction: Int): Boolean {
        return isSwipable && super.canScrollHorizontally(direction)
    }

    fun goForward() { currentItem++ }
    fun goBack() { currentItem-- }

}