package app.cheddar.wallet.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import app.cheddar.wallet.R
import app.cheddar.wallet.utils.*
import kotlinx.android.synthetic.main.layout_number_pad.view.*

open class CheddarNumberPad : FrameLayout {

    data class ButtonItem(val view: View, val value: String)

    private var itemClickListener: ((String) -> Unit)? = null
    private var deleteClickListener: (() -> Unit)? = null
    private val buttonItems by lazy {
        listOf(
            ButtonItem(oneButton, "1"),
            ButtonItem(twoButton, "2"),
            ButtonItem(threeButton, "3"),
            ButtonItem(fourButton, "4"),
            ButtonItem(fiveButton, "5"),
            ButtonItem(sixButton, "6"),
            ButtonItem(sevenButton, "7"),
            ButtonItem(eightButton, "8"),
            ButtonItem(nineButton, "9"),
            ButtonItem(dotButton, "."),
            ButtonItem(zeroButton, "0"),
            ButtonItem(deleteButton, "")
        )
    }

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        inflateAndAttach(R.layout.layout_number_pad)
        buttonItems.forEach { item ->
            item.view.press()
            item.view.setOnClickListener {
                val isBackspace = item.value.isEmpty()
                if (isBackspace) deleteClickListener?.invoke()
                else itemClickListener?.invoke(item.value)
            }
        }
    }

    fun setOnItemClickListener(itemClickListener: (String) -> Unit, deleteClickListener: () -> Unit) {
        this.itemClickListener = itemClickListener
        this.deleteClickListener = deleteClickListener
    }

}