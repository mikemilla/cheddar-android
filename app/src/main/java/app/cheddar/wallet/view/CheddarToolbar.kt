package app.cheddar.wallet.view

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.DrawableCompat
import app.cheddar.wallet.R
import app.cheddar.wallet.utils.*

open class CheddarToolbar : androidx.appcompat.widget.Toolbar {

    var menuRes: Int? = null
        set(value) {
            field = value
            if (menuRes != null && menuRes != 0) {
                menu.clear()
                inflateMenu(menuRes!!)
            }
        }

    var menuTintColor: Int = context.colorBackgroundInverse
        set(value) {
            field = value
            setMenuItemTint()
        }

    var showBackButton: Boolean = true
        set(value) {
            field = value
            navigationIcon = if (showBackButton) context.iconBack else null
        }

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        context.theme.obtainStyledAttributes(attrs, R.styleable.CheddarToolbar, defStyle, 0).apply {
            try {
                menuRes = getResourceId(R.styleable.CheddarToolbar_menu, 0)
                menuTintColor = getColor(R.styleable.CheddarToolbar_tintColor, context.colorBackgroundInverse)
                showBackButton = getBoolean(R.styleable.CheddarToolbar_showBackButton, true)
            } finally {
                recycle()
            }
        }

        setNavigationOnClickListener {
            (context as? AppCompatActivity)?.onBackPressed()
        }
    }

    private fun setMenuItemTint() {
        for (i in 0 until menu.size()) {
            val item = menu.getItem(i)
            item.icon = item.icon.mutate().apply {
                DrawableCompat.setTint(this, menuTintColor)
            }
        }
    }

}