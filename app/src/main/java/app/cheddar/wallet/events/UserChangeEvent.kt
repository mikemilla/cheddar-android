package app.cheddar.wallet.events

import app.cheddar.wallet.model.User

data class UserChangeEvent(val newUser: User)