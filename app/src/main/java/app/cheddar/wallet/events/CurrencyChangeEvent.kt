package app.cheddar.wallet.events

import app.cheddar.wallet.model.User

data class CurrencyChangeEvent(val newCurrency: String)