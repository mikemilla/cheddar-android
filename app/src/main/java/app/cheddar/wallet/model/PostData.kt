package app.cheddar.wallet.model

import app.cheddar.wallet.R
import com.google.gson.Gson

data class PostData(
    val data: Any
) {
    fun toData(): String {
        val data = Gson().toJson(data)
        return "{ \"data\" : $data }"
    }
}

data class Signup(
    val email: String,
    val password: String
)

data class Username(
    val username: String
)