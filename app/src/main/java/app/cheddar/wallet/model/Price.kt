package app.cheddar.wallet.model

import app.cheddar.wallet.App
import app.cheddar.wallet.utils.UserManager
import app.cheddar.wallet.utils.toDeviceCurrency
import java.text.NumberFormat
import java.util.*

data class Price(
    val aud: FiatCurrency? = null,
    val brl: FiatCurrency? = null,
    val cad: FiatCurrency? = null,
    val chf: FiatCurrency? = null,
    val clp: FiatCurrency? = null,
    val cny: FiatCurrency? = null,
    val dkk: FiatCurrency? = null,
    val eur: FiatCurrency? = null,
    val gbp: FiatCurrency? = null,
    val hkd: FiatCurrency? = null,
    val inr: FiatCurrency? = null,
    val isk: FiatCurrency? = null,
    val jpy: FiatCurrency? = null,
    val krw: FiatCurrency? = null,
    val nzd: FiatCurrency? = null,
    val pln: FiatCurrency? = null,
    val rub: FiatCurrency? = null,
    val sek: FiatCurrency? = null,
    val sgd: FiatCurrency? = null,
    val thb: FiatCurrency? = null,
    val twd: FiatCurrency? = null,
    val usd: FiatCurrency? = null
) : CheddarModel() {

    val currentFiatCurrency: FiatCurrency?
        get() {
            return when (UserManager.defaultFiatCurrency) {
                "aud" -> aud
                "brl" -> brl
                "cad" -> cad
                "chf" -> chf
                "clp" -> clp
                "cny" -> cny
                "dkk" -> dkk
                "eur" -> eur
                "gbp" -> gbp
                "hkd" -> hkd
                "inr" -> inr
                "isk" -> isk
                "jpy" -> jpy
                "krw" -> krw
                "nzd" -> nzd
                "pln" -> pln
                "rub" -> rub
                "sek" -> sek
                "sgd" -> sgd
                "thb" -> thb
                "twd" -> twd
                else -> usd
            }
        }

}

data class FiatCurrency(
    val amount: Double? = null,
    val symbol: String? = null
) : CheddarModel() {

    fun toValue(coinAmount: Double? = 0.0): String {
        val properValue = (coinAmount ?: 0.0) * (this.amount ?: 0.0)
        return properValue.toDeviceCurrency(properValue < 0.01)
    }

}