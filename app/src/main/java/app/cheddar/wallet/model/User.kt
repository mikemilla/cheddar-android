package app.cheddar.wallet.model

import app.cheddar.wallet.R
import java.util.*

data class User(
    val balances: List<Balance>? = emptyList(),
    val createdAt: Date? = null,
    val updatedAt: Date? = null,
    val username: String? = null
) : CheddarModel()