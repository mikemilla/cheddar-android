package app.cheddar.wallet.model

import app.cheddar.wallet.R
import java.util.*

data class Balance(
    val symbol: String,
    val totalBalance: Double,
    val availableBalance: Double
)