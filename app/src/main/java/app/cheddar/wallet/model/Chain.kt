package app.cheddar.wallet.model

import app.cheddar.wallet.R
import java.util.*

data class Chain(
    val createdAt: Date? = null,
    val updatedAt: Date? = null,
    val blockDuration: Int? = null,
    val blockHeight: Int? = null,
    val feeRate: Double? = null,
    val requiredConfirmations: Int? = null,
    val satoshiAddress: String? = null,
    val symbol: String? = null,
    val name: String? = null,
    val tokenName: String? = null,
    var price: Price? = null
) : CheddarModel()