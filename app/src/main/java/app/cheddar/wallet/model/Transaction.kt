package app.cheddar.wallet.model

import app.cheddar.wallet.repository.Auth
import java.util.*

data class Transaction(
    val symbol: String? = null,
    val isOnchain: Boolean? = null,
    val amount: Double? = null,
    val fee: Double? = null,
    val blockHeight: Int? = null,
    val blockId: String? = null,
    val transactionId: String? = null,
    val status: String? = null, // TODO
    val message: String? = null,
    val sendingUserId: String? = null,
    val receivingUserId: String? = null,
    val createdAt: Date? = null,
    val updatedAt: Date? = null
): CheddarModel() {

    val isCurrentUserSender get() = Auth.currentUser?.uid == sendingUserId
    val isCurrentUserReceiver get() = Auth.currentUser?.uid == receivingUserId

}