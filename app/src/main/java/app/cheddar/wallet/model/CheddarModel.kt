package app.cheddar.wallet.model

import app.cheddar.wallet.utils.toModel
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.Source

open class CheddarModel(var id: String? = null)