package app.cheddar.wallet.model

import app.cheddar.wallet.R

data class CheddarError(
    var exception: Exception = Exception(),
    var messageRes: Int = R.string.error_generic
): Throwable()