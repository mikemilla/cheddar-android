package app.cheddar.wallet.model

import app.cheddar.wallet.R
import java.util.*

data class Wallet(
    val createdAt: Date? = null,
    val updatedAt: Date? = null
): CheddarModel()