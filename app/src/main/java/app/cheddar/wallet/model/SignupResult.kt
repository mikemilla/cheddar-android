package app.cheddar.wallet.model

import app.cheddar.wallet.R

data class SignupResult(
    val success: Boolean,
    val userId: String,
    val token: String
)