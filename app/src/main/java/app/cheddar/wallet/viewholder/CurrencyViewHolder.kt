package app.cheddar.wallet.viewholder

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.cheddar.wallet.R
import app.cheddar.wallet.utils.*
import kotlinx.android.synthetic.main.item_currency.view.*

class CurrencyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    companion object {
        fun newInstance(parent: ViewGroup) =
            CurrencyViewHolder(parent.inflate(R.layout.item_currency))
    }

    fun bind(currency: String, isSelected: Boolean, onClick: (String) -> Unit) {
        itemView.apply {
            titleTextView.text = currency
            update(isSelected)
            root.press()
            root.setOnClickListener { onClick(currency) }
        }
    }

    fun update(isSelected: Boolean) {
        itemView.apply {
            if (isSelected) imageView.show()
            else imageView.hide()
        }
    }
}