package app.cheddar.wallet.viewholder

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.cheddar.wallet.R
import app.cheddar.wallet.model.Balance
import app.cheddar.wallet.model.Chain
import app.cheddar.wallet.utils.*
import app.cheddar.wallet.view.CheddarCurrencyView
import kotlinx.android.synthetic.main.item_wallet.view.*

class WalletViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    companion object {
        fun newInstance(parent: ViewGroup) =
            WalletViewHolder(parent.inflate(R.layout.item_wallet))
    }

    private var balance: Balance? = null
    private var chain: Chain? = null

    fun bind(chain: Chain, balance: Balance, onViewAddressClick: () -> Unit) {

        this.balance = balance

        itemView.apply {

            // Set total amount
            // This is not the available amount
            // but the amount they have including pending
            val tokenName = chain.tokenName.orEmpty()
            val amount = balance.totalBalance
            availableBalanceTextView.text = resources.getPluralForToken(tokenName, amount)

            // Setup view address
            val name = chain.name.orEmpty()
            viewAddressButton.title = context.getString(R.string.action_view_address_type, name)
            viewAddressButton.setOnClickListener {

                // TODO
                val currentStyle = currencyView.size
                currencyView.size = if (currentStyle == CheddarCurrencyView.Size.LARGE) CheddarCurrencyView.Size.SMALL else CheddarCurrencyView.Size.LARGE


//                onViewAddressClick.invoke()
            }

            updateChain(false, chain)
        }
    }

    fun updateChain(animated: Boolean, chain: Chain) {

        this.chain = chain

        balance?.let { balance ->
            itemView.apply {

                // Get needed values
                val usablePrice = chain.price ?: UserManager.recentBitcoinPrice // Fallback
//                val fiatAmount = usablePrice?.currentFiatCurrency?.toValue(balance.totalBalance)

                currencyView.update(usablePrice, balance, animated)
            }
        }
    }
}