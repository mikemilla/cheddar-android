package app.cheddar.wallet.viewholder

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.cheddar.wallet.R
import app.cheddar.wallet.model.Chain
import app.cheddar.wallet.model.Transaction
import app.cheddar.wallet.utils.*
import kotlinx.android.synthetic.main.item_transaction.view.*

class TransactionViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    companion object {
        fun newInstance(parent: ViewGroup) =
            TransactionViewHolder(parent.inflate(R.layout.item_transaction))
    }

    private var transaction: Transaction? = null
    private var chain: Chain? = null

    fun bind(transaction: Transaction, chain: Chain, onClick: (Transaction) -> Unit) {

        this.transaction = transaction

        itemView.apply {

            val tx = this@TransactionViewHolder.transaction!!
            val iconRes = if (tx.isCurrentUserSender) R.drawable.ic_send else R.drawable.ic_receive
            imageView.setImageResource(iconRes)
            senderTextView.text = if (tx.isCurrentUserSender) context.getString(R.string.copy_you) else tx.sendingUserId ?: context.getString(R.string.copy_someone)
            receiverTextView.text = if (tx.isCurrentUserReceiver) context.getString(R.string.copy_you) else tx.receivingUserId ?: "an address"
            messageTextView.text = "${tx.message} ${tx.updatedAt.toString()}" // Use createdAt
            receiverAmountTextView.text = tx.amount.toCleanNumber()
            statusView.text = transaction.status
            container.press()
            container.setOnClickListener { onClick(tx) }

            updateChain(false, chain)
        }
    }

    fun updateChain(animated: Boolean, chain: Chain) {

        this.chain = chain

        transaction?.let { transaction ->
            itemView.apply {

                // Get needed values
                val usablePrice = chain.price ?: UserManager.recentBitcoinPrice // Fallback
                val fiatAmount = usablePrice?.currentFiatCurrency?.toValue(transaction.amount) // TODO: Change currency
                val incrementRes = if (transaction.isCurrentUserReceiver) R.string.copy_plus_amount else R.string.copy_minus_amount

                // Update UI
                currencyAmountTextView.apply {
                    val newString = context.getString(incrementRes, fiatAmount)
                    if (text.toString() != newString) {
                        if (animated) {
                            hide(animated = true, onComplete = {
                                text = newString
                                show(animated = true)
                            })
                        } else text = newString
                    }
                }
            }
        }
    }
}