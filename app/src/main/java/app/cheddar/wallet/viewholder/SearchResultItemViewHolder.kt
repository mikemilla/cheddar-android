package app.cheddar.wallet.viewholder

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.cheddar.wallet.R
import app.cheddar.wallet.adapter.SearchResultAdapter
import app.cheddar.wallet.utils.inflate
import app.cheddar.wallet.utils.press
import kotlinx.android.synthetic.main.item_search_result.view.*

class SearchResultItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    companion object {
        fun newInstance(parent: ViewGroup) =
            SearchResultItemViewHolder(parent.inflate(R.layout.item_search_result))
    }

    fun bind(offsetItem: SearchResultAdapter.OffsetItem, onClickOffsetItem: (SearchResultAdapter.OffsetAction) -> Unit) {
        itemView.apply {
            imageView.setImageResource(offsetItem.imageRes)
            val chainName = offsetItem.chain?.name
            titleTextView.text = context.getString(R.string.copy_scan_a_address, chainName)
            containerContainer.press(1f)
            containerContainer.setOnClickListener { onClickOffsetItem(offsetItem.action) }
        }
    }

    fun bind(user: String, onClickUser: (String) -> Unit) { // TODO move to user object
        itemView.apply {
//            imageView.setImageResource(imageUrl)
            titleTextView.text = user
            containerContainer.press(1f)
            containerContainer.setOnClickListener { onClickUser(user) }
        }
    }

}