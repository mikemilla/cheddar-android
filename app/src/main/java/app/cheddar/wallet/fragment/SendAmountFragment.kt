package app.cheddar.wallet.fragment

import androidx.lifecycle.Observer
import app.cheddar.wallet.R
import app.cheddar.wallet.activity.TransactionBuilderActivity
import app.cheddar.wallet.utils.*
import app.cheddar.wallet.view.CheddarCurrencyView
import app.cheddar.wallet.viewmodel.TransactionBuilderViewModel
import kotlinx.android.synthetic.main.activity_transaction_builder.*
import kotlinx.android.synthetic.main.fragment_send_amount.*

class SendAmountFragment : BaseFragment<TransactionBuilderViewModel>(
    R.layout.fragment_send_amount,
    TransactionBuilderViewModel::class
) {

    private enum class InputState { FIAT, CRYPTO }

    private var inputState = InputState.FIAT // TODO: User Default?
        set(value) {
            field = value
            when (inputState) {
                InputState.FIAT -> {
                    currencyAmountView.size = CheddarCurrencyView.Size.LARGE
                    cryptoAmountView.size = CheddarCurrencyView.Size.SMALL
                    currencyAmountView.isActivated = true
                    cryptoAmountView.isActivated = false

                    currencyAmountView.animateHeightChange(
                        rootActivity.dimen88,
                        CheddarCurrencyView.ANIM_DURATION
                    )
                    cryptoAmountView.animateHeightChange(
                        rootActivity.dimen56,
                        CheddarCurrencyView.ANIM_DURATION
                    )

                }
                InputState.CRYPTO -> {
                    currencyAmountView.size = CheddarCurrencyView.Size.SMALL
                    cryptoAmountView.size = CheddarCurrencyView.Size.LARGE
                    currencyAmountView.isActivated = false
                    cryptoAmountView.isActivated = true

                    currencyAmountView.animateHeightChange(
                        rootActivity.dimen56,
                        CheddarCurrencyView.ANIM_DURATION
                    )
                    cryptoAmountView.animateHeightChange(
                        rootActivity.dimen88,
                        CheddarCurrencyView.ANIM_DURATION
                    )
                }
            }
        }

    val rootActivity get() = activity as TransactionBuilderActivity

    companion object {
        fun newInstance() = SendAmountFragment()
    }

    override fun onLayoutReady() {
        super.onLayoutReady()

        cryptoAmountView.press(1f)
        currencyAmountView.press(1f)
        inputState = InputState.FIAT

        currencyAmountView.setOnClickListener {
            inputState = InputState.FIAT
        }

        cryptoAmountView.setOnClickListener {
            inputState = InputState.CRYPTO
        }

        numberPad.setOnItemClickListener(
            itemClickListener = { character ->
                currencyAmountView.addCharacter(character)
                cryptoAmountView.addCharacter(character)
            },
            deleteClickListener = {
                currencyAmountView.removeCharacter()
                cryptoAmountView.removeCharacter()
            })

        cryptoAmountView.setOnValueChangeListener { value ->
            nextButton.isDisabled = value == 0.0
        }

        useMaxButton.press()
        useMaxButton.setOnClickListener {
            val balance = viewModel?.balanceLiveData?.value
            val value = balance?.availableBalance.toString()
            currencyAmountView.setValue(value)
            cryptoAmountView.setValue(value)
        }

        nextButton.setOnClickListener {

            val amount = cryptoAmountView.value
            val tokenName = viewModel?.chainLiveData?.value?.tokenName
            val title =
                if (inputState == InputState.CRYPTO) resources.getPluralForToken(tokenName, amount)
                else getFiatForAmount(amount)

            val payload = TransactionBuilderViewModel.PayloadData(
                value = cryptoAmountView.value,
                title = title,
                isFiat = inputState == InputState.FIAT
            )

            viewModel?.payloadLiveData?.postValue(payload)
            rootActivity.flowPager.goForward()
        }
    }

    override fun onViewModelReady(viewModel: TransactionBuilderViewModel) {
        super.onViewModelReady(viewModel)

        viewModel.chainLiveData.observe(viewLifecycleOwner, Observer { chain ->
            chainTitleTextView.text = getString(R.string.copy_send_value, chain.name)
        })

        viewModel.balanceLiveData.observe(viewLifecycleOwner, Observer { balance ->
            availableBalanceTextView.text =
                getString(R.string.copy_balance_available, balance.availableBalance.toString())
        })

        viewModel.test.observe(viewLifecycleOwner, Observer { isLoading ->
            val test = 123
        })

    }

}