package app.cheddar.wallet.fragment

import androidx.lifecycle.Observer
import app.cheddar.wallet.R
import app.cheddar.wallet.activity.TransactionBuilderActivity
import app.cheddar.wallet.adapter.SearchResultAdapter
import app.cheddar.wallet.utils.getPluralForToken
import app.cheddar.wallet.utils.hideKeyboard
import app.cheddar.wallet.utils.showKeyboard
import app.cheddar.wallet.utils.toastTodo
import app.cheddar.wallet.viewmodel.TransactionBuilderViewModel
import kotlinx.android.synthetic.main.activity_transaction_builder.*
import kotlinx.android.synthetic.main.fragment_receiver.*
import kotlinx.android.synthetic.main.fragment_send_amount.*
import org.jetbrains.anko.toast
import java.util.*

class ReceiverFragment : BaseFragment<TransactionBuilderViewModel>(
    R.layout.fragment_receiver,
    TransactionBuilderViewModel::class
) {

    val rootActivity get() = activity as TransactionBuilderActivity

    private val adapter by lazy {
        SearchResultAdapter(
            onClickOffsetAction = { action ->
                context?.toastTodo()
            },
            onClickUser = { user ->
                context?.toast(user)
            })
    }

    companion object {
        fun newInstance() = ReceiverFragment()
    }

    override fun onLayoutReady() {
        super.onLayoutReady()

        resultsRecyclerView.adapter = adapter

        adapter.results = listOf("mike", "dave", "bill", "chuck") // tODO
    }

    override fun onViewModelReady(viewModel: TransactionBuilderViewModel) {
        super.onViewModelReady(viewModel)

        viewModel.pageChangeLiveData.observe(viewLifecycleOwner, Observer { fragment ->
            if (fragment == this) receiverEditText.showKeyboard()
            else receiverEditText.hideKeyboard()
        })

        viewModel.payloadLiveData.observe(viewLifecycleOwner, Observer { payload ->
            sendValueTextView.text = getString(R.string.copy_send_value, payload.title)
        })

        viewModel.chainLiveData.observe(viewLifecycleOwner, Observer { chain ->
            val name = chain.name?.toLowerCase(Locale.getDefault())
            receiverEditText.hint = getString(R.string.copy_receiver_hint, name)
            adapter.chain = chain
        })

    }

}