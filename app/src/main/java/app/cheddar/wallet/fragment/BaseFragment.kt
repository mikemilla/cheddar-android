package app.cheddar.wallet.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import app.cheddar.wallet.viewmodel.TransactionBuilderViewModel
import kotlin.reflect.KClass

abstract class BaseFragment<VM : ViewModel>(
    private val layoutResourceId: Int,
    private val viewModelClass: KClass<VM>? = null
) : Fragment() {

    private var isLayoutReady = false
    var baseView: View? = null
    var viewModel: VM? = null
        set(value) {
            field = value
            if (viewModel != null && isLayoutReady) {
                onViewModelReady(viewModel!!)
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (baseView == null) {
            baseView = inflater.inflate(layoutResourceId, container, false)
        }
        return baseView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Call on view ready
        // Only called once
        baseView?.let {
            if (!isLayoutReady) {
                isLayoutReady = true
                onLayoutReady()
            }
        }

        // Post the view model if needed
        if (viewModel != null && isLayoutReady) {
            onViewModelReady(viewModel!!)
        }

        // Called every time the view is shown
        onLayoutAppear()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        onLayoutDisappear()
    }

    protected open fun onViewModelReady(viewModel: VM) {}
    protected open fun onLayoutReady() {}
    open fun onLayoutAppear() {}
    open fun onLayoutDisappear() {}

}