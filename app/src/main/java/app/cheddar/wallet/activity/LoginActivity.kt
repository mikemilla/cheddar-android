package app.cheddar.wallet.activity

import android.os.Bundle
import androidx.lifecycle.Observer
import app.cheddar.wallet.Navigator
import app.cheddar.wallet.R
import app.cheddar.wallet.utils.SnackBarUtils
import app.cheddar.wallet.utils.toMessage
import app.cheddar.wallet.viewmodel.AuthViewModel
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_signup.emailInput
import kotlinx.android.synthetic.main.activity_signup.passwordInput

class LoginActivity : ProgressActivity<AuthViewModel>(R.layout.activity_login, AuthViewModel::class) {

    override fun onLayoutReady(savedInstanceState: Bundle?) {
        super.onLayoutReady(savedInstanceState)

        contentLayout = content

        loginButton.setOnClickListener {
            val email = emailInput.text.toString()
            val password = passwordInput.text.toString()
            viewModel.login(email, password)
        }
    }

    override fun onViewModelReady(viewModel: AuthViewModel) {
        super.onViewModelReady(viewModel)

        viewModel.isLoadingLiveData.observe(this, Observer { isLoading ->
            if (isLoading) showProgress()
        })

        viewModel.errorLiveData.observe(this, Observer { error ->
            showContent()
            SnackBarUtils.present(this, snackBarContainer, error.toMessage())
        })

        viewModel.userLiveData.observe(this, Observer { user ->
            when (user.username) {
                null -> Navigator.toChangeUsername(this, true)
                else -> Navigator.toMain(this)
            }
        })

    }

}
