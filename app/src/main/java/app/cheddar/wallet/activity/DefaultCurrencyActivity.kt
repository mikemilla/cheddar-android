package app.cheddar.wallet.activity

import android.os.Bundle
import androidx.lifecycle.Observer
import app.cheddar.wallet.Navigator
import app.cheddar.wallet.R
import app.cheddar.wallet.adapter.CurrenciesAdapter
import app.cheddar.wallet.adapter.TransactionsAdapter
import app.cheddar.wallet.repository.Auth
import app.cheddar.wallet.utils.SnackBarUtils
import app.cheddar.wallet.utils.fillEdgeToEdge
import app.cheddar.wallet.utils.toMessage
import app.cheddar.wallet.utils.toastTodo
import app.cheddar.wallet.viewmodel.DefaultCurrencyViewModel
import app.cheddar.wallet.viewmodel.SettingsViewModel
import app.cheddar.wallet.viewmodel.WalletViewModel
import kotlinx.android.synthetic.main.activity_default_currency.*
import org.jetbrains.anko.toast

class DefaultCurrencyActivity :
    ProgressActivity<DefaultCurrencyViewModel>(
        R.layout.activity_default_currency,
        DefaultCurrencyViewModel::class
    ) {

    private var bottomEdge = 0
    private val adapter by lazy {
        CurrenciesAdapter(recyclerView, onCurrencyClick = { currency ->
            viewModel.updateCurrency(currency)
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        root.fillEdgeToEdge { top, bottom ->
            bottomEdge = bottom
            toolbar.setPadding(0, top, 0, 0)
            recyclerView.setPadding(0, 0, 0, bottom)
        }
    }

    override fun onLayoutReady(savedInstanceState: Bundle?) {
        super.onLayoutReady(savedInstanceState)
        recyclerView.adapter = adapter
    }

    override fun onViewModelReady(viewModel: DefaultCurrencyViewModel) {
        super.onViewModelReady(viewModel)

        viewModel.requiredDataLiveData.observe(this, Observer { data ->
            adapter.build(data.defaultCurrency, data.currencies)
        })

    }

}
