package app.cheddar.wallet.activity

import android.os.Bundle
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.ViewModel
import androidx.viewpager.widget.ViewPager
import app.cheddar.wallet.R
import app.cheddar.wallet.fragment.BaseFragment
import app.cheddar.wallet.fragment.ReceiverFragment
import app.cheddar.wallet.fragment.SendAmountFragment
import app.cheddar.wallet.model.Balance
import app.cheddar.wallet.model.Chain
import app.cheddar.wallet.utils.delay
import app.cheddar.wallet.utils.parse
import app.cheddar.wallet.viewmodel.TransactionBuilderViewModel
import kotlinx.android.synthetic.main.activity_transaction_builder.*
import kotlinx.android.synthetic.main.fragment_send_amount.*
import kotlin.reflect.KClass

class TransactionBuilderActivity :
    ProgressActivity<TransactionBuilderViewModel>(
        R.layout.activity_transaction_builder,
        TransactionBuilderViewModel::class
    ) {

    companion object {
        const val CHAIN = "chain"
        const val BALANCE = "balance"
    }

    private val intentChain: Chain? by lazy {
        intent.getStringExtra(CHAIN).parse<Chain>()
    }

    private val intentBalance: Balance? by lazy {
        intent.getStringExtra(BALANCE).parse<Balance>()
    }

    private val pages = listOf(
        Page("Send Bitcoin", SendAmountFragment.newInstance()),
        Page("Send Something", ReceiverFragment.newInstance())
    )

    override fun onLayoutReady(savedInstanceState: Bundle?) {
        super.onLayoutReady(savedInstanceState)

//        // Setup toolbar
//        toolbar.apply {
//            setOnMenuItemClickListener { menuItem ->
//                when (menuItem.itemId) {
//                    R.id.action_settings -> Navigator.toSettings(this@SendAmountActivity)
//                }
//                return@setOnMenuItemClickListener false
//            }
//        }
//
//        contentLayout = snackBarContainer
//        recyclerView.adapter = adapter

        flowPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                val fragment = pages.map { it.fragment }[position]
                viewModel.pageChangeLiveData.postValue(fragment)
            }
        })
        flowPager.adapter = PageAdapter(supportFragmentManager).apply {
            this.pages = this@TransactionBuilderActivity.pages
        }

//        numberPad.setOnItemClickListener { value, shouldRemoveLast ->
//            toast("$value :: $shouldRemoveLast")
//        }
    }

    override fun onBackPressed() {
        when {
            flowPager.currentItem != 0 -> flowPager.goBack()
            else -> super.onBackPressed() // TODO: Ensure we are safe
        }
    }

    override fun onViewModelReady(viewModel: TransactionBuilderViewModel) {
        super.onViewModelReady(viewModel)

        viewModel.build(intentChain, intentBalance)

        pages.forEach {
            it.fragment.viewModel = viewModel
        }

        delay(2000) {
            viewModel.test.value = true
        }

    }

    data class Page<VM : ViewModel>(val title: String, val fragment: BaseFragment<VM>)

    inner class PageAdapter(fragmentManager: FragmentManager) :
        FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        var pages = listOf<Page<*>>()
            set(value) {
                field = value
                notifyDataSetChanged()
            }

        override fun getItem(position: Int) = pages[position].fragment
        override fun getPageTitle(position: Int) = pages[position].title
        override fun getCount() = pages.size
    }

}
