package app.cheddar.wallet.activity

import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import app.cheddar.wallet.utils.registerIfNeeded
import app.cheddar.wallet.view.CheddarToolbar
import org.greenrobot.eventbus.EventBus
import kotlin.reflect.KClass

abstract class BaseActivity<VM : ViewModel>(
    private val layoutResourceId: Int?,
    private val viewModelClass: KClass<VM>? = null
) : AppCompatActivity() {

    val eventBus get() = EventBus.getDefault()
    private var _viewModel: VM? = null
    protected var viewModel: VM
        get() = _viewModel!!
        set(value) {
            _viewModel = value
        }

    /**
     * Overriding classes should use onLayoutReady or onViewModelReady
     * instead of onCreate to ensure app is fully initialized
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setLayout(layoutResourceId, savedInstanceState)
        setViewModel(viewModelClass)
    }

    private fun setLayout(layoutResourceId: Int?, savedInstanceState: Bundle?) {
        layoutResourceId?.let {
            setContentView(layoutResourceId)
            onLayoutReady(savedInstanceState)
        }
    }

    private fun setViewModel(viewModelClass: KClass<VM>?) {
        viewModelClass?.let {
            viewModel = ViewModelProviders.of(this).get(viewModelClass.java)
            onViewModelReady(viewModel)
        }
    }

    protected open fun onLayoutReady(savedInstanceState: Bundle?) {
        // Empty for optional override
    }

    protected open fun onViewModelReady(viewModel: VM) {
        // Empty for optional override
    }

    override fun onStart() {
        super.onStart()
        eventBus.registerIfNeeded(this)
    }

    override fun onResume() {
        super.onResume()
        eventBus.registerIfNeeded(this)
    }

    override fun onRestart() {
        super.onRestart()
        eventBus.registerIfNeeded(this)
    }

    fun showDarkStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = window.decorView.systemUiVisibility and
                    View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
        }
    }

    fun showLightStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = window.decorView.systemUiVisibility or
                    View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
    }
}