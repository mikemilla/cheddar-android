package app.cheddar.wallet.activity

import android.os.Bundle
import androidx.lifecycle.Observer
import app.cheddar.wallet.Navigator
import app.cheddar.wallet.R
import app.cheddar.wallet.repository.Auth
import app.cheddar.wallet.utils.*
import app.cheddar.wallet.viewmodel.SettingsViewModel
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity :
    ProgressActivity<SettingsViewModel>(R.layout.activity_settings, SettingsViewModel::class) {

    private var bottomEdge = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        root.fillEdgeToEdge { top, bottom ->
            bottomEdge = bottom
            toolbar.setPadding(0, top, 0, 0)
            scrollViewContent.setPadding(0, 0, 0, bottom)
        }
    }

    override fun onLayoutReady(savedInstanceState: Bundle?) {
        super.onLayoutReady(savedInstanceState)

        contentLayout = snackBarContainer

        usernameItem.press()
        usernameItem.setOnClickListener {
            Navigator.toChangeUsername(this)
        }

        defaultCurrencyItem.press()
        defaultCurrencyItem.setOnClickListener {
            Navigator.toDefaultCurrency(this)
        }

        logoutItem.press()
        logoutItem.setOnClickListener {
            Auth.signOut()
            Navigator.toWelcome(this)
        }

    }

    override fun onViewModelReady(viewModel: SettingsViewModel) {
        super.onViewModelReady(viewModel)

        viewModel.currentUserLiveData.observe(this, Observer { user ->
            currentUsernameTextView.text = user.username
        })

        viewModel.currentFiatCurrencyLiveData.observe(this, Observer { currency ->
            selectedCurrencyTextView.text = currency
        })

    }

}
