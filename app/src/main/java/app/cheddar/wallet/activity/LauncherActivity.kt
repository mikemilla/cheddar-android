package app.cheddar.wallet.activity

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import app.cheddar.wallet.Navigator
import app.cheddar.wallet.R
import app.cheddar.wallet.repository.Auth
import app.cheddar.wallet.viewmodel.LauncherViewModel
import com.google.firebase.FirebaseApp

import kotlinx.android.synthetic.main.activity_wallet.*

class LauncherActivity :
    BaseActivity<LauncherViewModel>(R.layout.activity_launcher, LauncherViewModel::class) {

    override fun onViewModelReady(viewModel: LauncherViewModel) {
        super.onViewModelReady(viewModel)

        viewModel.requiredDataLiveData.observe(this, Observer { requiredData ->
            when {
                requiredData.isUserSignedIn -> {
                    if (requiredData.user?.username == null) Navigator.toChangeUsername(this, true)
                    else Navigator.toMain(this)
                }
                else -> Navigator.toWelcome(this)
            }
        })

    }

}
