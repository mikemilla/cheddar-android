package app.cheddar.wallet.activity

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModel
import app.cheddar.wallet.utils.hide
import app.cheddar.wallet.utils.show
import kotlinx.android.synthetic.main.include_fallback_empty.*
import kotlinx.android.synthetic.main.include_fallback_error.*
import kotlinx.android.synthetic.main.include_fallback_progress.*
import kotlin.reflect.KClass

abstract class ProgressActivity<VM : ViewModel>(
    layoutResourceId: Int,
    viewModelClass: KClass<VM>? = null
) : BaseActivity<VM>(
    layoutResourceId,
    viewModelClass) {

    enum class ViewState {
        LOADING, EMPTY, ERROR, CONTENT
    }

    var contentLayout: View? = null
    var emptyLayout: View? = null
    var errorLayout: View? = null
    var progressLayout: View? = null
    var viewState = ViewState.LOADING
        private set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        errorRetryButton?.setOnClickListener(this::onClickRetry)
    }

    open fun showError() {
        viewState = ViewState.ERROR
        setErrorViewIfNeeded()
        errorLayout?.show(true)
        hideContent()
        hideEmpty()
        hideProgress()
    }

    open fun hideError() {
        setErrorViewIfNeeded()
        errorLayout?.hide(true)
    }

    open fun showEmpty() {
        viewState = ViewState.EMPTY
        setEmptyViewIfNeeded()
        emptyLayout?.show(true)
        hideContent()
        hideError()
        hideProgress()
    }

    open fun hideEmpty() {
        setEmptyViewIfNeeded()
        emptyLayout?.hide(true)
    }

    open fun shouldShowEmpty(shouldShow: Boolean) {
        if (shouldShow) showEmpty() else hideEmpty()
    }

    open fun showProgress() {
        viewState = ViewState.LOADING
        setProgressViewIfNeeded()
        progressLayout?.show(true)
        hideContent()
        hideError()
        hideEmpty()
    }

    open fun hideProgress() {
        setProgressViewIfNeeded()
        progressLayout?.hide(true)
    }

    open fun showContent() {
        viewState = ViewState.CONTENT
        contentLayout?.show(true)
        hideProgress()
        hideError()
        hideEmpty()
    }

    open fun hideContent() {
        contentLayout?.hide(true)
    }

    open fun onClickRetry(view: View?) {
        // Empty for optional override
    }

    private fun setProgressViewIfNeeded() {
        if (progressLayout == null) {
            progressLayout = fallbackProgressView
        }
    }

    private fun setEmptyViewIfNeeded() {
        if (emptyLayout == null) {
            emptyLayout = fallbackEmptyView
        }
    }

    private fun setErrorViewIfNeeded() {
        if (errorLayout == null) {
            errorLayout = fallbackErrorView
        }
    }

}