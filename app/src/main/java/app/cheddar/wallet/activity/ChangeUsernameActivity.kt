package app.cheddar.wallet.activity

import android.os.Bundle
import androidx.lifecycle.Observer
import app.cheddar.wallet.Navigator
import app.cheddar.wallet.R
import app.cheddar.wallet.utils.SnackBarUtils
import app.cheddar.wallet.utils.moveCursorToEnd
import app.cheddar.wallet.utils.toMessage
import app.cheddar.wallet.utils.toastTodo
import app.cheddar.wallet.viewmodel.ChangeUsernameViewModel
import kotlinx.android.synthetic.main.activity_change_username.*

class ChangeUsernameActivity : ProgressActivity<ChangeUsernameViewModel>(R.layout.activity_change_username, ChangeUsernameViewModel::class) {

    companion object {
        const val SHOW_BACK_BUTTON = "show_back_button"
    }

    private val showBackButton by lazy {
        intent.getBooleanExtra(SHOW_BACK_BUTTON, true)
    }

    override fun onLayoutReady(savedInstanceState: Bundle?) {
        super.onLayoutReady(savedInstanceState)

        // Remove back button if needed
        if (!showBackButton) toolbar.navigationIcon = null

        saveUsernameButton.setOnClickListener {
            val username = usernameInput.text.toString()
            viewModel.changeUsername(username)
        }

    }

    override fun onViewModelReady(viewModel: ChangeUsernameViewModel) {
        super.onViewModelReady(viewModel)

        viewModel.isLoadingLiveData.observe(this, Observer { isLoading ->
            if (isLoading) showProgress()
        })

        viewModel.errorLiveData.observe(this, Observer { error ->
            showContent()
            SnackBarUtils.present(this, snackBarContainer, error.toMessage())
        })

        viewModel.currentUserLiveData.observe(this, Observer { currentUser ->
            usernameInput.apply {
                setText(currentUser.username)
                moveCursorToEnd()
            }
        })

        viewModel.updatedUserLiveData.observe(this, Observer { user ->
            if (!showBackButton) Navigator.toMain(this)
            else finish()
        })

    }

}
