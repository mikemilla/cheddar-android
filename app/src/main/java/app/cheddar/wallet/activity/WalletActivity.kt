package app.cheddar.wallet.activity

import android.os.Bundle
import androidx.lifecycle.Observer
import app.cheddar.wallet.Navigator
import app.cheddar.wallet.R
import app.cheddar.wallet.adapter.TransactionsAdapter
import app.cheddar.wallet.model.Balance
import app.cheddar.wallet.model.User
import app.cheddar.wallet.utils.*
import app.cheddar.wallet.viewmodel.WalletViewModel
import kotlinx.android.synthetic.main.activity_wallet.*

class WalletActivity :
    ProgressActivity<WalletViewModel>(R.layout.activity_wallet, WalletViewModel::class) {

    private var bottomEdge = 0
    private val adapter by lazy {
        TransactionsAdapter(
            recyclerView = recyclerView,
            onViewAddressClick = {
                toastTodo()
            },
            onTransactionClickItem = { transaction ->
                SnackBarUtils.present(
                    this,
                    snackBarContainer,
                    transaction.id.toString(),
                    bottomEdge
                )
            })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        root.fillEdgeToEdge { top, bottom ->
            bottomEdge = bottom
            toolbar.setPadding(0, top, 0, 0)
            recyclerView.setPadding(0, 0, 0, bottom)
        }
    }

    override fun onLayoutReady(savedInstanceState: Bundle?) {
        super.onLayoutReady(savedInstanceState)

        // Setup toolbar
        toolbar.apply {
            setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.action_settings -> Navigator.toSettings(this@WalletActivity)
                }
                return@setOnMenuItemClickListener false
            }
        }

        sendFab.setOnClickListener {
            val chain = viewModel.chainLiveData.value ?: return@setOnClickListener
            val balance = viewModel.currentBalance ?: return@setOnClickListener
            Navigator.toTransactionBuilder(this, chain, balance)
        }

        contentLayout = snackBarContainer
        recyclerView.adapter = adapter

        refreshLayout.setOnRefreshListener {
            viewModel.refreshPrice()
        }
    }

    override fun onViewModelReady(viewModel: WalletViewModel) {
        super.onViewModelReady(viewModel)

        viewModel.isLoadingLiveData.observe(this, Observer { isLoading ->
            if (isLoading) showProgress()
        })

        viewModel.errorLiveData.observe(this, Observer { error ->
            showContent()
            SnackBarUtils.present(this, snackBarContainer, error.toMessage())
        })

        viewModel.currentUserLiveData.observe(this, Observer { user ->
            updateUsername(user)
        })

        viewModel.currentFiatCurrencyLiveData.observe(this, Observer { currency ->
            adapter.notifyDataSetChanged()
        })

        viewModel.chainLiveData.observe(this, Observer { chain ->
            adapter.updateChain(chain)
            refreshLayout.isRefreshing = false
        })

        viewModel.requiredDataLiveData.observe(this, Observer { data ->

            // Set username
            updateUsername(data.user)

            // Set wallet and txs
            val chain = data.chain
//            val balance = data.user?.balances?.firstOrNull()
            val balance = Balance("btc_reg", 1.0, 1.0)

            // TODO

//            val txs = data.transactions.orEmpty().filter { it.amount ?: 0.0 > 1 }
            val txs = data.transactions.orEmpty()
            adapter.update(chain, balance, txs)

            showContent()

        })

    }

    // Sets the username for the current user
    private fun updateUsername(user: User?) {
        user?.username?.let { username ->
            val title = getString(R.string.copy_username_with_at, username)
            usernameTitle.text = title
        }
    }

}
