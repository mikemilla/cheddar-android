package app.cheddar.wallet.activity

import android.os.Bundle
import androidx.lifecycle.ViewModel
import app.cheddar.wallet.Navigator
import app.cheddar.wallet.R

import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : BaseActivity<ViewModel>(R.layout.activity_welcome) {

    override fun onLayoutReady(savedInstanceState: Bundle?) {
        super.onLayoutReady(savedInstanceState)

        loginButton.setOnClickListener {
            Navigator.toLogin(this)
        }

        saveUsernameButton.setOnClickListener {
            Navigator.toSignup(this)
        }

    }

}
