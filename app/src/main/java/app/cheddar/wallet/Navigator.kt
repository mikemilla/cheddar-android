package app.cheddar.wallet

import android.content.Context
import android.content.Intent
import android.content.Intent.*
import androidx.appcompat.app.AppCompatActivity
import app.cheddar.wallet.activity.*
import app.cheddar.wallet.model.Balance
import app.cheddar.wallet.model.Chain
import app.cheddar.wallet.utils.toJson

object Navigator {

    fun toMain(context: Context?) {
        if (context is AppCompatActivity) {
            Intent(context, WalletActivity::class.java).apply {
                flags = FLAG_ACTIVITY_NEW_TASK or FLAG_ACTIVITY_CLEAR_TASK
                context.startActivity(this)
            }
            context.finish()
        }
    }

    fun toTransactionBuilder(context: Context?, chain: Chain, balance: Balance) {
        Intent(context, TransactionBuilderActivity::class.java).apply {
            putExtra(TransactionBuilderActivity.CHAIN, chain.toJson())
            putExtra(TransactionBuilderActivity.BALANCE, balance.toJson())
            context?.startActivity(this)
        }
    }

    fun toWelcome(context: Context?) {
        if (context is AppCompatActivity) {
            Intent(context, WelcomeActivity::class.java).apply {
                if (context is LauncherActivity) {
                    context.startActivity(this)
                    context.finish()
                } else {
                    addFlags(FLAG_ACTIVITY_NEW_TASK or FLAG_ACTIVITY_CLEAR_TASK or FLAG_ACTIVITY_CLEAR_TOP)
                    context.startActivity(this)
                }
            }
            context.finish()
        }
    }

    fun toLogin(context: Context?) {
        Intent(context, LoginActivity::class.java).apply {
            context?.startActivity(this)
        }
    }

    fun toSignup(context: Context?) {
        Intent(context, SignupActivity::class.java).apply {
            context?.startActivity(this)
        }
    }

    fun toSettings(context: Context?) {
        Intent(context, SettingsActivity::class.java).apply {
            context?.startActivity(this)
        }
    }

    fun toDefaultCurrency(context: Context?) {
        Intent(context, DefaultCurrencyActivity::class.java).apply {
            context?.startActivity(this)
        }
    }

    fun toChangeUsername(context: Context?, finishCurrentActivity: Boolean = false) {
        Intent(context, ChangeUsernameActivity::class.java).apply {

            // Set needed flags
            if (finishCurrentActivity) {
                flags = FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_TASK_ON_HOME
                putExtra(ChangeUsernameActivity.SHOW_BACK_BUTTON, false)
            }

            // Launch
            context?.startActivity(this)
        }

        // Finish current activity if needed
        if (finishCurrentActivity && context is AppCompatActivity) {
            context.finish()
        }
    }

}