package app.cheddar.wallet.viewmodel

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import app.cheddar.wallet.events.CurrencyChangeEvent
import app.cheddar.wallet.events.UserChangeEvent
import app.cheddar.wallet.fragment.BaseFragment
import app.cheddar.wallet.model.*
import app.cheddar.wallet.repository.*
import app.cheddar.wallet.utils.Keys
import com.google.firebase.firestore.ListenerRegistration
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class TransactionBuilderViewModel : BaseViewModel() {

    data class PayloadData(
        val value: Double,
        val title: String,
        val isFiat: Boolean)

    val pageChangeLiveData = MutableLiveData<BaseFragment<*>>()
    val chainLiveData = MutableLiveData<Chain>()
    val balanceLiveData = MutableLiveData<Balance>()
    val payloadLiveData = MutableLiveData<PayloadData>()
    val test = MutableLiveData<Boolean>()
    val errorLiveData = MutableLiveData<CheddarError>()
    val isLoadingLiveData by lazy {
        MediatorLiveData<Boolean>().apply {
//            addSource(requiredDataLiveData) { if (it != null) value = false }
            addSource(errorLiveData) { if (it != null) value = false }
        }
    }

    init {

        isLoadingLiveData.value = true


    }

    fun build(chain: Chain?, balance: Balance?) {
        chainLiveData.value = chain
        balanceLiveData.value = balance
    }

//    // User Change
//    @Subscribe(threadMode = ThreadMode.ASYNC)
//    fun onEvent(event: UserChangeEvent) {
//        val newUser = event.newUser
//        currentUserLiveData.postValue(newUser)
//    }

}