package app.cheddar.wallet.viewmodel

import androidx.lifecycle.MutableLiveData
import app.cheddar.wallet.events.UserChangeEvent
import app.cheddar.wallet.model.*
import app.cheddar.wallet.utils.UserManager
import app.cheddar.wallet.view.CheddarCurrencyView
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class DefaultCurrencyViewModel : BaseViewModel() {

    data class RequiredData(
        val currencies: List<String>,
        val defaultCurrency: String
    )

    companion object {
        private val supportedCurrencies =
            listOf("usd", "eur", "gbp", "jpy", "cad", "aud", "cny")
    }

    val requiredDataLiveData = MutableLiveData<RequiredData>()

    init {
        val defaultCurrency = UserManager.defaultFiatCurrency
        val data = RequiredData(supportedCurrencies, defaultCurrency)
        requiredDataLiveData.postValue(data)
    }

    fun updateCurrency(currency: String) {
        UserManager.defaultFiatCurrency = currency
    }

}