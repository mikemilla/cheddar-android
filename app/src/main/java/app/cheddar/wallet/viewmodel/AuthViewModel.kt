package app.cheddar.wallet.viewmodel

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import app.cheddar.wallet.App
import app.cheddar.wallet.R
import app.cheddar.wallet.model.*
import app.cheddar.wallet.repository.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception

class AuthViewModel : BaseViewModel() {

    private val authRepo = AuthRepository()
    private val usersRepo = UsersRepository()

    val userLiveData = MutableLiveData<User>()
    val errorLiveData = MutableLiveData<CheddarError>()
    val isLoadingLiveData by lazy {
        MediatorLiveData<Boolean>().apply {
            addSource(userLiveData) { if (it != null) value = false }
            addSource(errorLiveData) { if (it != null) value = false }
        }
    }

    fun signup(email: String, password: String) = GlobalScope.launch {

        when {
            email.isBlank() -> {
                val e = Exception(App.instance.getString(R.string.error_email_empty))
                val error = CheddarError(e)
                errorLiveData.postValue(error)
                return@launch
            }
            password.isBlank() -> {
                val e = Exception(App.instance.getString(R.string.error_password_empty))
                val error = CheddarError(e)
                errorLiveData.postValue(error)
                return@launch
            }
        }

        // Create new user
        // Log them in
        // Get the current user data
        try {
            isLoadingLiveData.postValue(true)
            val userData = authRepo.signup(email, password)
            val authResult = authRepo.login(userData.token)
            val user = usersRepo.getSelf()
            userLiveData.postValue(user)
        } catch (e: CheddarError) {
            errorLiveData.postValue(e)
        }
    }

    fun login(email: String, password: String) {

        when {
            email.isBlank() -> {
                val e = Exception(App.instance.getString(R.string.error_email_empty))
                val error = CheddarError(e)
                errorLiveData.postValue(error)
                return
            }
            password.isBlank() -> {
                val e = Exception(App.instance.getString(R.string.error_password_empty))
                val error = CheddarError(e)
                errorLiveData.postValue(error)
                return
            }
        }

        // Log in
        // Get current user data
        isLoadingLiveData.value = true
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val authResult = authRepo.login(email, password)
                val user = usersRepo.getSelf()
                userLiveData.postValue(user)
            } catch (e: CheddarError) {
                errorLiveData.postValue(e)
            }
        }
    }

}