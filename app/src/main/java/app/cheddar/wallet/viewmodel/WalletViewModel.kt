package app.cheddar.wallet.viewmodel

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import app.cheddar.wallet.events.CurrencyChangeEvent
import app.cheddar.wallet.events.UserChangeEvent
import app.cheddar.wallet.model.*
import app.cheddar.wallet.repository.*
import app.cheddar.wallet.utils.Keys
import com.google.firebase.firestore.ListenerRegistration
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class WalletViewModel : BaseViewModel() {

    data class RequiredData(
        val chain: Chain?,
        val user: User?,
        val transactions: List<Transaction>?)

    private val usersRepo = UsersRepository()
    private val walletsRepo = WalletsRepository()
    private val chainsRepo = ChainsRepository()
    private var walletListener: ListenerRegistration? = null
    private var chainsListener: ListenerRegistration? = null

    val symbol = Keys.btcReg // TODO
    val currentBalance get() = requiredDataLiveData.value?.user?.balances?.firstOrNull()

    val currentFiatCurrencyLiveData = MutableLiveData<String>()
    val currentUserLiveData = MutableLiveData<User>()
    val chainLiveData = MutableLiveData<Chain>()
    val requiredDataLiveData = MutableLiveData<RequiredData>()
    val errorLiveData = MutableLiveData<CheddarError>()
    val isLoadingLiveData by lazy {
        MediatorLiveData<Boolean>().apply {
            addSource(requiredDataLiveData) { if (it != null) value = false }
            addSource(errorLiveData) { if (it != null) value = false }
        }
    }

    init {

        isLoadingLiveData.value = true

        chainsListener = chainsRepo.setOnChainChangeListener(
            symbol = symbol,
            onSuccess = { chainLiveData.postValue(it) },
            onFailure = { errorLiveData.postValue(it) })

        walletListener = walletsRepo.setOnWalletChangeListener(
            symbol = symbol,
            onSuccess = {

                // Fetch all the required data concurrently
                GlobalScope.launch(Dispatchers.Default) {
                    try {
                        val chain = async { chainLiveData.value ?: chainsRepo.getChain(symbol) }
                        val user = async { usersRepo.getSelf() }
                        val transactions = async { walletsRepo.getTransactions(symbol) }
                        val data = RequiredData(chain.await(), user.await(), transactions.await())
                        requiredDataLiveData.postValue(data)
                    } catch (e: CheddarError) {
                        errorLiveData.postValue(e)
                    }
                }

            },
            onFailure = {
                errorLiveData.postValue(it)
            })
    }

    // User Change
    @Subscribe(threadMode = ThreadMode.ASYNC)
    fun onEvent(event: UserChangeEvent) {
        val newUser = event.newUser
        currentUserLiveData.postValue(newUser)
    }

    // Currency Change
    @Subscribe(threadMode = ThreadMode.ASYNC)
    fun onEvent(event: CurrencyChangeEvent) {
        val newCurrency = event.newCurrency
        currentFiatCurrencyLiveData.postValue(newCurrency)
    }

    fun refreshPrice() = GlobalScope.launch(Dispatchers.Default) {
        val chain = chainsRepo.getChain(symbol)
        chainLiveData.postValue(chain)
    }

    override fun onCleared() {
        super.onCleared()
        walletListener?.remove()
        chainsListener?.remove()
    }

}