package app.cheddar.wallet.viewmodel

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import app.cheddar.wallet.model.*
import app.cheddar.wallet.repository.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class LauncherViewModel : BaseViewModel() {

    data class RequiredData(val isUserSignedIn: Boolean, val user: User?)

    private val usersRepo = UsersRepository()

    val requiredDataLiveData = MutableLiveData<RequiredData>()
    val errorLiveData = MutableLiveData<CheddarError>()
    val isLoadingLiveData by lazy {
        MediatorLiveData<Boolean>().apply {
            addSource(requiredDataLiveData) { if (it != null) value = false }
            addSource(errorLiveData) { if (it != null) value = false }
        }
    }

    init {
        load()
    }

    fun load() = GlobalScope.launch {
        val isUserSignedIn = Auth.isSignedIn
        val userData = async { usersRepo.getSelf() }
        val requiredData = RequiredData(isUserSignedIn, userData.await())
        requiredDataLiveData.postValue(requiredData)
    }

}