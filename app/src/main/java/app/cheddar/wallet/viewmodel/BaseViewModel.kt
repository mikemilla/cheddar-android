package app.cheddar.wallet.viewmodel

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.cheddar.wallet.R
import app.cheddar.wallet.model.*
import app.cheddar.wallet.repository.*
import app.cheddar.wallet.utils.*
import com.google.firebase.firestore.ListenerRegistration
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus

open class BaseViewModel : ViewModel() {

    private val eventBus get() = EventBus.getDefault()

    init {
        eventBus.registerIfNeeded(this)
    }

    override fun onCleared() {
        super.onCleared()
        eventBus.unregisterIfNeeded(this)
    }

}