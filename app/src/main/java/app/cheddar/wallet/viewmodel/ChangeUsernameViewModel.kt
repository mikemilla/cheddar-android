package app.cheddar.wallet.viewmodel

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import app.cheddar.wallet.model.*
import app.cheddar.wallet.repository.UsersRepository
import app.cheddar.wallet.utils.UserManager

class ChangeUsernameViewModel : BaseViewModel() {

    private val usersRepo = UsersRepository()

    val currentUserLiveData = MutableLiveData<User>()
    val updatedUserLiveData = MutableLiveData<User>()
    val errorLiveData = MutableLiveData<CheddarError>()
    val isLoadingLiveData by lazy {
        MediatorLiveData<Boolean>().apply {
            addSource(updatedUserLiveData) { if (it != null) value = false }
            addSource(errorLiveData) { if (it != null) value = false }
        }
    }

    init {
        val currentUser = UserManager.currentUser
        currentUserLiveData.postValue(currentUser)
    }

    fun changeUsername(newUsername: String) {
        usersRepo.changeUsername(newUsername,
            onSuccess = { updatedUserLiveData.postValue(it) },
            onFailure = { errorLiveData.postValue(it) })
    }

}