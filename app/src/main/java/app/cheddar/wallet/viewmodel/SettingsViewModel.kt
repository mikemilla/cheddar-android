package app.cheddar.wallet.viewmodel

import androidx.lifecycle.MutableLiveData
import app.cheddar.wallet.events.CurrencyChangeEvent
import app.cheddar.wallet.events.UserChangeEvent
import app.cheddar.wallet.model.*
import app.cheddar.wallet.utils.UserManager
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class SettingsViewModel : BaseViewModel() {

    val currentUserLiveData = MutableLiveData<User>()
    val currentFiatCurrencyLiveData = MutableLiveData<String>()

    init {

        // Get the current user
        val currentUser = UserManager.currentUser
        currentUserLiveData.postValue(currentUser)

        // Get the current currency
        val currentFiat = UserManager.defaultFiatCurrency
        currentFiatCurrencyLiveData.postValue(currentFiat)
    }

    // User change
    @Subscribe(threadMode = ThreadMode.ASYNC)
    fun onEvent(event: UserChangeEvent) {
        val newUser = event.newUser
        currentUserLiveData.postValue(newUser)
    }

    // Currency Change
    @Subscribe(threadMode = ThreadMode.ASYNC)
    fun onEvent(event: CurrencyChangeEvent) {
        val newCurrency = event.newCurrency
        currentFiatCurrencyLiveData.postValue(newCurrency)
    }

}