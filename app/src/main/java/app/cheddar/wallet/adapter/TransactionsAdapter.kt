package app.cheddar.wallet.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.cheddar.wallet.model.Balance
import app.cheddar.wallet.model.Chain
import app.cheddar.wallet.model.Transaction
import app.cheddar.wallet.utils.getAllVisibleIndexes
import app.cheddar.wallet.utils.getViewHolderAtIndex
import app.cheddar.wallet.viewholder.TransactionViewHolder
import app.cheddar.wallet.viewholder.WalletViewHolder

class TransactionsAdapter(
    private val recyclerView: RecyclerView,
    private val onViewAddressClick: () -> Unit,
    private val onTransactionClickItem: (Transaction) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val WALLET_VIEW_TYPE = 0
        private const val TRANSACTION_VIEW_TYPE = 1
    }

    var chain: Chain? = null
        private set
    var balance: Balance? = null
        private set
    var transactions: List<Transaction>? = null
        private set
    var pendingChainUpdate: Chain? = null
        private set
    var currentScrollState: Int = RecyclerView.SCROLL_STATE_IDLE
        private set(value) {

            // Can handle pending update
            if (field != value && value == RecyclerView.SCROLL_STATE_IDLE && pendingChainUpdate != null) {
                this.chain = pendingChainUpdate
                updateVisibleItems(pendingChainUpdate!!)
                this.pendingChainUpdate = null
            }

            field = value
        }

    val isInitialized get() = transactions != null && chain != null
    private val compensation get() = if (balance != null) 1 else 0

    init {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                currentScrollState = newState
            }
        })
    }

    override fun getItemCount(): Int = transactions.orEmpty().size + compensation

    override fun getItemViewType(position: Int): Int {
        return when {
            position == 0 && balance != null -> WALLET_VIEW_TYPE
            else -> TRANSACTION_VIEW_TYPE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            WALLET_VIEW_TYPE -> WalletViewHolder.newInstance(parent)
            else -> TransactionViewHolder.newInstance(parent)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val index = holder.adapterPosition
        when (holder) {
            is WalletViewHolder -> {
                if (isInitialized) {
                    holder.bind(chain!!, balance!!, onViewAddressClick)
                }
            }
            is TransactionViewHolder -> {
                if (isInitialized) {
                    val txIndex = index - compensation
                    val transaction = transactions.orEmpty()[txIndex]
                    holder.bind(transaction, chain!!, onTransactionClickItem)
                }
            }
        }
    }

    fun update(newChain: Chain?, newBalance: Balance?, newTransactions: List<Transaction>) {

        if (!isInitialized) {
            this.chain = newChain
            this.balance = newBalance
            this.transactions = newTransactions
            notifyDataSetChanged()
            return
        }

        // Reloading index 0 TODO
        if (this.balance != newBalance) {
            this.balance = newBalance
            notifyItemChanged(0)
        }

        // Get the txs indexes that need to be updated
        val stalePairs = this.transactions.orEmpty().map { tx -> tx.id to tx.updatedAt }
        val freshPairs = newTransactions.map { tx -> tx.id to tx.updatedAt }
        val staleIds = stalePairs.map { it.first }
        val txsToRefresh = freshPairs.minus(stalePairs).map { pair ->
            val id = pair.first
            val index = staleIds.indexOf(id)
            val shouldRefresh = staleIds.contains(id)
            return@map index to shouldRefresh
        }

        // Set the new transactions
        this.transactions = newTransactions

        // Reload all txs
        var numberOfInserts = 0
        txsToRefresh.forEach { pair ->
            val shouldRefresh = pair.second
            if (shouldRefresh) {
                val index = pair.first + compensation + numberOfInserts
                notifyItemChanged(index)
            } else {
                numberOfInserts++
                notifyItemInserted(compensation)
            }
        }
    }

    // Attempt to update the chain data
    // If the user is scrolling the list we temp cache the chain
    // and attempt to update when user is done scrolling
    fun updateChain(chain: Chain) {
        if (isInitialized) {
            if (currentScrollState == RecyclerView.SCROLL_STATE_IDLE) {
                this.chain = chain
                updateVisibleItems(chain)
            } else {
                pendingChainUpdate = chain
            }
        }
    }

    private fun updateVisibleItems(chain: Chain) {
        val indexesToUpdate = recyclerView.getAllVisibleIndexes()
        indexesToUpdate.forEach { index ->
            if (index == 0) { // TODO
                val viewHolder = recyclerView.getViewHolderAtIndex<WalletViewHolder>(index)
                viewHolder?.updateChain(true, chain)
            } else {
                val viewHolder = recyclerView.getViewHolderAtIndex<TransactionViewHolder>(index)
                viewHolder?.updateChain(true, chain)
            }
        }
    }

}