package app.cheddar.wallet.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.cheddar.wallet.model.Balance
import app.cheddar.wallet.model.Chain
import app.cheddar.wallet.model.Transaction
import app.cheddar.wallet.utils.Keys
import app.cheddar.wallet.utils.getAllVisibleIndexes
import app.cheddar.wallet.utils.getViewHolderAtIndex
import app.cheddar.wallet.viewholder.CurrencyViewHolder
import app.cheddar.wallet.viewholder.TransactionViewHolder
import app.cheddar.wallet.viewholder.WalletViewHolder

class CurrenciesAdapter(
    private val recyclerView: RecyclerView,
    private val onCurrencyClick: (String) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var selectedCurrency: String = Keys.usd
        private set
    var currencies: List<String> = emptyList()
        private set

    override fun getItemCount(): Int = currencies.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = CurrencyViewHolder.newInstance(parent)
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val index = holder.adapterPosition
        when (holder) {
            is CurrencyViewHolder -> {
                val currency = currencies[index]
                val isSelected = selectedCurrency == currency
                holder.bind(currency, isSelected, onClick = { newCurrency ->
                    selectCurrency(newCurrency)
                })
            }
        }
    }

    private fun selectCurrency(newCurrency: String) {
        if (selectedCurrency != newCurrency) {

            // Deselect previous
            val prevIndex = currencies.indexOf(selectedCurrency)
            recyclerView.getViewHolderAtIndex<CurrencyViewHolder>(prevIndex)?.update(false)

            // Select new
            val newIndex = currencies.indexOf(newCurrency)
            recyclerView.getViewHolderAtIndex<CurrencyViewHolder>(newIndex)?.update(true)

            // Notify change
            selectedCurrency = newCurrency
            onCurrencyClick(selectedCurrency)
        }
    }

    fun build(selectedCurrency: String, currencies: List<String>) {
        this.selectedCurrency = selectedCurrency
        this.currencies = currencies
        notifyDataSetChanged()
    }

}