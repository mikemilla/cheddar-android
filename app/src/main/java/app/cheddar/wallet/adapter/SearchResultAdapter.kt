package app.cheddar.wallet.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.cheddar.wallet.R
import app.cheddar.wallet.model.Chain
import app.cheddar.wallet.viewholder.SearchResultItemViewHolder
import app.cheddar.wallet.viewholder.TransactionViewHolder
import app.cheddar.wallet.viewholder.WalletViewHolder

class SearchResultAdapter(
    private val onClickOffsetAction: (OffsetAction) -> Unit,
    private val onClickUser: (String) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    enum class OffsetAction { SCAN_ADDRESS }

    data class OffsetItem(
        val action: OffsetAction,
        val imageRes: Int,
        val titleRes: Int,
        val chain: Chain?
    )

    companion object {
        private const val HEADER_ITEM = 0
        private const val RESULT_ITEM = 1
    }

    private val offsetList
        get() = listOf(
            OffsetItem(
                action = OffsetAction.SCAN_ADDRESS,
                imageRes = R.drawable.ic_send,
                titleRes = R.string.copy_scan_a_address,
                chain = chain
            )
        )

    var chain: Chain? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var results: List<String> = emptyList()
        set(value) {
            field = value
//            notifyItemRangeChanged(1, value.size)
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int = results.size + offsetList.size

    override fun getItemViewType(position: Int): Int {
        return when {
            position <= offsetList.lastIndex -> HEADER_ITEM
            else -> RESULT_ITEM
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SearchResultItemViewHolder.newInstance(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val index = holder.adapterPosition
        when (holder) {
            is SearchResultItemViewHolder -> {
                if (index <= offsetList.lastIndex) {
                    val offsetItemAtIndex = offsetList[index]
                    holder.bind(offsetItemAtIndex, onClickOffsetAction)
                } else {
                    val i = index - offsetList.size
                    val resultAtIndex = results[i]
                    holder.bind(resultAtIndex, onClickUser)
                }
            }
        }
    }

}