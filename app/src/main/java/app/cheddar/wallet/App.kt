package app.cheddar.wallet

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import app.cheddar.wallet.repository.ChainsRepository
import app.cheddar.wallet.repository.Firestore
import app.cheddar.wallet.utils.Keys
import app.cheddar.wallet.utils.UserManager
import com.google.firebase.firestore.ListenerRegistration
import org.greenrobot.eventbus.EventBus
import timber.log.Timber

class App : Application() {

    companion object {
        lateinit var instance: App
        const val MAX_CURRENCY_DECIMAL_PLACES = 4
    }

    override fun onCreate() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        super.onCreate()
        instance = this
        Timber.plant(Timber.DebugTree())
        Firestore.build()
    }

}